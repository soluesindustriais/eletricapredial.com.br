<? $h1 = "Painéis de Energia Elétrica"; $title  = " Painéis de Energia Elétrica"; $desc = "O painel de energia elétrica pode ser a melhor opção para a sua indústria, solicite um orçamento no site da Elétrica Predial e confira as vantagens deste portal!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O <strong>painel elétrico de comando</strong> e montagem industrial pode ser definido como um compartimento modular utilizado para alocar dispositivos eletrônicos em seu interior. Geralmente, os painéis são construídos em estruturas em chapa metálica, com perfis de dobras perfurados ou não, possuindo fechamentos em chapas e portas com sistema de fecho.
							Atualmente, existem uma série de normas que regularizam todos os processos desenvolvidos por máquinas como essas, por exemplo, os <strong>painéis elétricos de baixa tensão</strong> são protocolados pelas NBR IEC 60439-1, assim como ela, são criadas muitas outras que também ajudam a manter um padrão de funcionamento, trazendo até mais segurança para a empresa e colaborador.
							Além disso, outro método de proteger a corporação que está cada vez mais sendo adotado por empresas do mundo inteiro são os ensaios de rotina em painéis elétricos. Nele é feita uma série de inspeções do conjunto, da instalação elétrica, e em alguns casos um ensaio de funcionamento.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Quadro de Comando Elétrico</h2>
						<p>Que os painéis elétricos são equipamentos extremamente versáteis todo mundo sabe, mas muitos ainda não conhecem todas as aplicações que ele possui. Ele pode ser utilizado para partidas diretas de motores, partidas com soft starters, um dispositivo desenvolvido para suavizar a partido do motor diminuindo o pico de corrente quando o mesmo está deixando o ponto de inércia.
							Além desses, esse equipamento também se enquadra facilmente em máquinas de partidas com inversores de frequência, um aparelho que auxilia no controle da velocidade de operação, além de estabelecer rampas de aceleração e desaceleração dos motores.
						Outra finalidade possível é a junção desse dispositivo com os painéis CLP, uma espécie de computador que possibilita a automação de diversos sistemas que podem ser facilmente alterados, basta alterar a programação que está dentro da memória deste equipamento.</p>
						<h2>Montagem de Painel Elétrico</h2>
						<p>Ao optar por utilizar esse equipamento na indústria, muitas pessoas iniciam um processo desgastante de análise e reconhecimento de empresas que forneçam ou fabriquem esse aparelho, mas isso não é necessário, atualmente existem uma série de marketplaces onde os maiores nomes do segmento industrial se reúnem para oferecer serviços ao consumidor.
						A utilização é muito fácil, o cliente só precisa acessar o site, digitar o nome do produto desejado e solicitar uma cotação, simples assim. Confira!</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg" alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-eletrico.php');?>
<button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar">Orçamento Grátis</button>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>