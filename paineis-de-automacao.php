<? $h1 = "Painéis de automação"; $title  = "Painéis Elétricos Industriais | Elétrica Predial"; $desc = "Fique atento sobre as principais novidades do ramo industrial e aproveite para realizar um orçamento com as melhores empresas do segmento. Acesse agora!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Os <strong>painéis automatizados</strong> são dispositivos que vão além do painel elétrico, devido a tecnologia investida no funcionamento deste aparelho, sua composição e funcionalidade conseguem superar os tradicionais modelos de painéis de comando.
							Geralmente, equipamentos com esse nível de tecnologia, possuem em sua composição um dispositivo conhecido como painel CLP, ele funciona como o cérebro de toda a operação. Todas as atividades que serão elaboradas pelo equipamento em que o painel elétrico está ligado, serão processadas primeiro por ele, e a partir de então encaminhadas para os demais dispositivos da máquina para que as mesmas sejam executadas.
							Investir em tecnologia só traz benefícios para a empresa, pois já que todos os processos serão realizados de forma automática o funcionário não precisará entrar em contato com produtos químicos nocivos, correndo menos riscos à saúde.
							Além disso, através do <strong>painel de controle</strong> a máquina será programada para realizar os mais diversos tipos de processos, depois que todos os parâmetros estiverem de acordo com o planejado, o equipamento irá executá-los com total precisão, garantindo maior qualidade para os produtos do cliente.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Instalações Elétricas Industriais</h2>
						<p>Independente de sua aplicação, esse maquinário possui a mesma função, controlar as funções de um determinada aparelho, não importando qual seja o segmento da empresa onde o mesmo será instalado ou o tipo de máquina na qual ele estará conectado.
						Porém, conforme são fornecidas mais informações à CPU do dispositivo, mais conhecimento e autonomia a máquina ganha. Isso fará com que os processo sejam executados de maneira mais ágil e precisa, por isso é tão importante adquirir um produto como o <strong>painel elétrico industrial</strong>.</p>
						<h2>Montagem de painéis elétricos</h2>
						<p>A montagem dos painéis elétricos deve ser realizada por uma empresa especialista no assunto e que acima de tudo, entenda o funcionamento do equipamento e conheça as instalações do cliente, só dessa forma será possível desenvolver um dispositivo 100% personalizado.
						Não perca tempo entrando em vários sites para conseguir realizar uma cotação, confie em um dos maiores portais voltados para a indústria e encontre os melhores <strong>fabricantes de painel elétrico</strong> com um clique.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg" alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-eletrico.php');?>
<button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar">Orçamento Grátis</button>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>