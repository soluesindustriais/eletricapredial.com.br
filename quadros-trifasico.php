<? $h1 = "Quadros trifásico"; $title  = "Caixa de Distribuição | Elétrica Predial"; $desc = "Sabia que o quadro trifásico pode melhorar o funcionamento da sua indústria? Para saber mais acesse o site da Elétrica Predial e faça um orçamento agora!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O <strong>quadro elétrico trifásico</strong> tem como objetivo efetuar o acionamento de aparelhos que utilizem o motor, como bombas e centrífugas, ou sistema trifásico como base, o painel elétrico trifásico é um dos utensílios mais empregados no mercado em inúmeros segmentos industriais. O quadro trifásico é importante para os procedimentos de partida do motor, é imprescindível levar em conta as propriedades de maquinários que melhor se adapta, tendo em vista o propósito de preservar a potência obtida no nível impecável.
							O <strong>quadro trifásico</strong> é bem similar aos quadros monofásico, isso porque os dois tipos de equipamentos praticamente exercem a função, que é ministrar níveis de tensão variados para máquinas e aparelhos diferentes.
							Além de sua diversidade de uso e benefícios da instalação é importante destacar que o <strong>quadro de distribuição trifásico</strong> pode ser usado em shopping centers, aeroportos, hotéis e centros comerciais, em estações de tratamento de água e esgoto, e no geral em todas as industriais.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Quadro de distribuição</h2>
						<p>Os <strong>quadros de distribuição elétrica</strong> são sistemas instalados internamente. Esses tipos de painéis são responsáveis pelo acionamento de máquinas e equipamentos com a distribuição de eletricidade para os diversos pontos. Por meio deles, é possível também comandar todo o sistema elétrico, em que chaves são acionadas evitando sobrecargas de energia e consequentemente acidentes elétricos.
						Os sistemas que comandam a eletricidade de uma empresa ou indústria devem funcionar perfeitamente e necessitam de atenção. O quadro de distribuição com barramento trifásico é preferido por oferecer instalações seguras e resistentes.</p>
						<h2>Montagem de quadro elétrico trifásico</h2>
						<p>Os <strong>quadros elétricos</strong> trifásicos são equipamentos presentes em quase toda instalação, seja ela industrial, predial ou residencial.
							Para que a montagem dos quadros elétricos proporcione a solução perfeita alguns aspectos são de total importância. O primeiro deles é o cumprimento das normas vigentes, sendo a principal delas a NBR5410. A conformidade com as normas técnicas e de segurança acrescenta a garantia de bom funcionamento e proteção para o sistema e para pessoas. Outro ponto muito importante é a escolha de materiais utilizados, pois quadros elétricos precisam ser construídos com materiais resistentes e componentes de qualidade.
						</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg" alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					<? include('inc/galeria-fixa-mpi.php');?>
				</div>
				<? include('inc/form-mpi.php');?>
			</section>
		</div>
	</main>
</div>
<? include('inc/footer.php');?>
</body>
</html>