<? $h1 = "Painéis elétricos";
$title  = " Quadro Elétrico | Elétrica Predial";
$desc = "Aparelho primordial para o funcionamento, os painéis elétricos são essenciais para o funcionamento de máquinas na indústria. Acesse o site e saiba mais!";
$key  = "Venda de quadros e painéis elétricos, Quadro de energia";
include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php');
include('inc/head.php');
 ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/painel-eletrico-1.jpg" alt="Painel Elétrico" title="Painel Elétrico" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O painel elétrico é o compartimento responsável por alocar inversores, contatores, chaves eletrônicas e dispositivos de entrada de sinais com o objetivo de controlar outros dispositivos eletrônicos utilizados em um ambiente que pode ser residencial ou comercial.
							Esse tipo de <strong>painel elétrico</strong> é responsável especificamente por distribuir a energia, existem alguns tipos de painéis e cada um tem uma função pré-estabelecida que são designados ao serviço desejado. O tipo de painel ou <strong>quadro elétrico</strong> ideal depende da complexidade do projeto e do uso que será feito, sendo essencial um alinhamento entre os engenheiros especialistas da obra.
							A utilização desses painéis elétricos é ampla e acaba se tornando mais que um simples quadro de distribuição, pois também é aplicado em centros de controle de motores (CCM), em mesas de comando e até mesmo em cabines de barramentos.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Montagem de painel elétrico</h2>
						<p>A <strong>montagem de painéis elétricos</strong> é solicitada pela indústria quando as atividades executadas, geralmente, em grande escala, precisam de um monitoramento de energia. A montagem desses painéis pode ser feita conforme a necessidade do negócio e quantidade de máquinas que a sua indústria tem para controlar.
							A montagem de painéis elétricos também pode ser para uma causa específica, algumas empresas solicitam esse serviço de montagem até mesmo para controlar o sistema de iluminação ou conseguir determinar quais máquinas tem prioridade nesse processo.
						Além disso essa instalação pode se moldar com a necessidade da sua indústria. Esses quadros elétricos conseguem se adaptar conforme o local de aplicação, que pode ser em mesa, armário, colunas, fixo, móvel, tudo pode variar conforme o lugar em que será adicionado o quadro.</p>
						<h2>Painéis elétricos industriais</h2>
						<p>Os painéis elétricos abrangem uma gama muito grande de uso sendo aplicado principalmente em processos que exigem a montagem elétrica industrial. Cada indústria necessita de uma instalação diferente, para suprir uma energia específica, a <strong>instalação de painéis</strong> garantem um bom funcionamento da parte elétrica industrial além de proteger essa rede e não colocar em risco a segurança de quem opera nesse ramo.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/painel-eletrico-2.jpg" alt="Painel Elétrico" title="Painel Elétrico" data-anime="in">
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-eletrico.php');?>
<button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar">Orçamento Grátis</button>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>