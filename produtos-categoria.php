<?
$h1         = 'Produtos';
$title      = 'Produtos';
$desc       = 'Produtos';
$key        = 'produtos, painel vinílico, painelepóxi';
$var        = 'painel';
include('inc/head.php');
?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
 <main>
    <div class="content">
    <?=$caminho;?>

    <h1>Produtos</h1>   
      <article class="full">   
        <p>encontre diferentes tipos de Paineis e revestimentos para suas necessidades, solicite agora mesmo um orçamento online com mais de 50 empresas ao mesmo tempo.</p>
        <ul class="thumbnails-main">
          <li>
            <a rel="nofollow" href="<?=$url?>painel-categoria" title="Painel"><img src="<?=$url?>imagens/comprar-quadro-eletronico.jpg" alt="painel" title="Painel"/></a>
            <h2><a href="<?=$url?>painel-categoria" title="Painel">Painel</a></h2>
          </li>

          <li>
            <a rel="nofollow" href="<?=$url?>painel-de-senha-categoria" title="Painel Se Senha"><img src="<?=$url?>imagens/comprar-painel-de-senha.jpg" alt="painel de senha" title="Painel Se Senha"/></a>
            <h2><a href="<?=$url?>painel-de-senha-categoria" title="Painel Se Senha">Painel De Senha</a></h2>
          </li>

          <li>
            <a rel="nofollow" href="<?=$url?>painel-eletrico-categoria" title="Painel Elétrico"><img src="<?=$url?>imagens/quadro-eletrico-industrial.jpg" alt="painel-eletrico" title="Painel Elétrico "/></a>
            <h2><a href="<?=$url?>painel-eletrico-categoria" title="Painel Elétrico">Painel Elétrico</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>painel-eletronico-categoria" title="Painel Eletrônico"><img src="<?=$url?>imagens/comprar-painel-eletronico.jpg" alt="painel-eletronico" title="Painel Eletrônico"/></a>
            <h2><a href="<?=$url?>painel-eletronico-categoria" title="painel eletronico">Painel Eletrônico</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>quadro-de-transferencia-categoria" title="Quadro de Trânsferencia"><img src="<?=$url?>imagens/quadro-eletronico-preco.jpg" alt="Quadro de Trânsferencia" title="Quadro de Trânsferencia"/></a>
            <h2><a href="<?=$url?>quadro-de-transferencia-categoria" title="Quadro de Trânsferencia">Quadro de Trânsferencia</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>quadro-eletrico-categoria" title="Quadro Elétrico"><img src="<?=$url?>imagens/comprar-quadros-eletronicos.jpg" alt="Quadro-eletrico" title="Quadro Elétrico"/></a>
            <h2><a href="<?=$url?>quadro-eletrico-categoria" title="Quadro Elétrico">Quadro Elétrico</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>manutencao-e-instalacao-eletrica-categoria" title="Manutenção e instalação eletrica"><img src="<?=$url?>imagens/comprar-quadro-eletronico.jpg" alt="Manutenção e instalção elétrica" title="Manutenção e instalção elétrica"/></a>
            <h2><a href="<?=$url?>manutencao-e-instalacao-eletrica-categoria" title="Manutenção e instalção elétrica">Manutenção e instalção elétrica</a></h2>
          </li>

        </ul>
      </section>
    </div>
  </main>

<!--   <main>
    <div class="content">
      <div id="breadcrumb" itemscope itemtype="http://schema.org/breadcrumb" >
        <a rel="home" itemprop="url" href="http://www.eletricapredial.com.br" title="home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> home</span></a> »
        <strong><span class="page" itemprop="title">Produtos</span></strong>
      </div>
      <h1>Produtos</h1>
      <p>O painel elétrico tem como função alocar, em seu interior, dispositivos eletrônicos. A grande parte desse tipos de painéis são as estruturas em chapa metálica, com perfis de dobras perfurados ou não, possuindo fechamentos em chapas e portas com sistema de fecho.</p>
      <p>É importante ressaltar também a forte resistência desse tipo de painel, pois eles possuem proteção anti-corrosiva que é formada pela superfície protetora externa.</p>
      <article class="full">
        <p>encontre diferentes tipos de Paineis e revestimentos para suas necessidades, solicite agora mesmo um orçamento online com mais de 50 empresas ao mesmo tempo.</p>
        <ul class="thumbnails-2">
          <li>
            <a  href="<?=$url?>paineis-eletricos" title="Painel Elétrico"><img src="<?=$url?>imagens/comprar-quadro-eletronico.jpg" alt="painel-eletrico" title="Painel"/></a>
            <h3><a href="<?=$url?>painel-categoria" title="Painel">Painel Elétrico</a></h3>
          </li>
          <li>
            <a href="<?=$url?>paineis-eletronicos" title="Painel Eletronico"><img src="<?=$url?>imagens/comprar-painel-de-senha.jpg" alt="painel-de-senha" title="Painel Se Senha"/></a>
            <h3><a href="<?=$url?>painel-de-senha-categoria" title="Painel Se Senha">Painel Eletrônico</a></h3>
          </li>
          <li>
            <a  href="<?=$url?>quadros-eletricos" title="Quadros Elétricos"><img src="<?=$url?>imagens/quadro-eletrico-industrial.jpg" alt="quadros-eletricos" title="Quadros Elétricos"/></a>
            <h3><a href="<?=$url?>painel-eletrico-categoria" title="Quadros Elétricos">Quadros Elétricos</a></h3>
          </li>
          <li>
            <a  href="<?=$url?>quadros-de-transferencia" title="Quadro de Transferência"><img src="<?=$url?>imagens/quadro-de-transferencia.jpg" alt="painel-eletronico" title="Quadro de Transferência"/></a>
            <h3><a href="<?=$url?>quadros-de-transferencia" title="painel eletronico">Quadro de Transferência</a></h3>
          </li>
        </ul>
<hr>

          <p>Além de todo o cuidado na instalação é importante que o painel esteja dentro das normas de NR exigidas. Os dispositivos e os circuitos deste conjunto devem ser dispostos de maneira que facilite a sua operação e reparo se </p>
        <br class="clear">
<div class="wrapper">
  <div class="fundo-video ">
    <div class="video-produto">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/_vsr3NhCMy0?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media"></iframe>
</div>
<h4 class="txtcenter">Painel Elétrico</h4>
  </div>  
</div>
        <? include('inc/form-mpi.php');?>
      </section>
    </div>
  </main> -->
</div>
<? include('inc/footer.php');?>
</body>
</html>