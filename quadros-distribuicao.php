<? $h1 = "Quadro de Distribuição Elétrica com Barramento"; $title  = "Painéis Elétricos | Elétrica Predial"; $desc = "Não perca a oportunidade de realizar orçamento agora com o melhor portal do ramo indústrial, lá você encontra produtos de qualidade com preços únicos. Acesse!
"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/quadro-eletrico-1.jpg" alt="quadro-eletrico" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							No dia a dia da indústria, esse é um dispositivo que pode passar despercebido por nossos olhos, mas é através do <strong>painel elétrico industrial</strong> que empresas de todos os segmentos conseguem coordenar as atividades realizadas por seus maquinários.
							O painel elétrico é composto por uma série de componentes que juntos formam um grande sistema, nele cada um exerce uma função diferente. Dessa forma, quando o operador ativa uma determinado processo, é responsabilidade desse compreender o comando e envia-lo para o equipamento, e assim atividade ser realizada, um processo muito semelhante ao realizado pelo nosso cérebro.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Painel elétrico</h2>
						<p>O <strong>painel de comando elétrico</strong> é um método utilizado em grande parte das empresas de ramo industrial como forma de controlar os equipamentos que estão em funcionamento. É através desse tipo de dispositivo que os colaboradores conseguem gerenciar os processos que acontecem na parte interna do aparelho.
							Não possuindo distinção de segmento, o <strong>quadro de distribuição elétrica</strong> pode ser acoplado a qualquer equipamento, a única variação, é que de acordo com a máquina onde ele será conectado e o funcionamento da linha de produção, esse produto poderá sofrer algumas mudanças, uma espécie de personalização.
							Em alguns segmentos da indústria, como a de alimentos, por exemplo, é comum encontrar máquinas onde o painel se resume a algumas luzes, sendo que cada uma indica alguma coisa, como o funcionamento do motor, sobrecarga, etc, além do famigerado botão de ligar e desligar.
						E em outros ambientes, como no tratamento térmico, onde é mais comum a injeção de produtos químicos nas peças, a análise da dureza do produto e mais uma série de processos, esse equipamento costuma apresentar uma série de informações que ajudarão o colaborador a coordenar as atividades seguintes.</p>
						<h2>Montador de Painéis Elétricos</h2>
						<p>Ficou curioso para saber como adquirir um <strong>quadro de distribuição</strong>? É muito simples, acesse um dos maiores portais indústrias da atualidade e confira todas as novidades disponíveis.
						Lá o cliente tem a oportunidade de descobrir uma série de empresas que fornecem o produto ou serviço que solucionará seus problemas, tudo isso com apenas um clique. Além disso, o consumidor ainda terá a oportunidade de solicitar uma cotação e obter respostas em tempo recorde. Aproveite!</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/quadro-eletrico-2.jpg" alt="quadro-eletrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-quadro-eletrico.php');?>

<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>