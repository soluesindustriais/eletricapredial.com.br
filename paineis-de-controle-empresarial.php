<? $h1 = "Painéis de Controle Empresarial"; $title  = "Painel de Comando Elétrico | Elétrica Predial
"; $desc = "Essencial para qualquer empresa industrial, os painéis elétricos são essenciais para o funcionamento de equipamentos. Acesse o site e solicite um orçamento!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Apesar de existirem alguns modelos padrões de <strong>painel de distribuição</strong> é muito difícil encontrar um que seja exatamente igual ao outro. Como cada máquina funciona de um modo diferente e cada linha de montagem exige que o aparelho seja controlado de uma forma única, os painéis são ajustados para suprir essas necessidades.
							Um dos produtos mais disseminados no dia a dia da indústria, o <strong>quadro de comando</strong> possui umas das principais funções dentro da fábrica, ele é o responsável por coordenar todas as atividades de uma determinada máquina para que ao final de todos os processos a mercadoria esteja dentro do padrão de qualidade da empresa.
							Para garantir que as atividades dele sejam realizadas com total qualidade, e identificar previamente qualquer tipo de falha, diversas empresas investem em um processo chamado de ensaio de rotina em painéis elétricos. Nele são inspecionados todos os componentes que preenchem o painel e em alguns casos é realizada um ensaio de funcionamento.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Painel Elétrico Industrial</h2>
						<p>Os <strong>quadros de distribuição</strong> são comuns na maioria das instalações industriais e edifícios comerciais ou residenciais. A maioria consiste em um painel elétrico de comando e montagem em gabinete alimentado com um cabo de alimentação de energia na entrada.
							A potência é então dividida entre vários pequenos disjuntores ou, no caso de painéis antigos, placas de distribuição, que por sua vez combinam potência de alimentação para diferentes pontos de consumo ou circuitos.
						A função principal de qualquer painel elétrico é permitir que os circuitos individuais possam extrair energia a partir de disjuntores corretamente classificados e isolados, para que quando houver uma falha, não haja o risco de causa uma interrupção para o resto da alimentação, mais importante do que isso, é a proteção contra choque elétrico ou incêndio que esse equipamento oferece.</p>
						<h2>Fabricante de Painéis Elétricos</h2>
						<p>Adquirir um equipamento dessa é mais fácil do que muito imaginam, atualmente existem uma série de portais que ajudam o consumidor a selecionar a melhor empresa para realizar um determinado serviço ou produzir uma certa mercadoria.
						Para isso o cliente só precisa acessar o site do marketplace, buscar pelo produto de interesse e solicitar uma cotação com as empresas que fornecem o produto desejado, tudo isso com apenas alguns cliques e sem precisar realizar cadastros.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg" alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-eletrico.php');?>
<button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar">Orçamento Grátis</button>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>