<? $h1 = "Painéis eletrônicos"; $title  = "Letreiro de LED | Elétrica Predial"; $desc = "Muito utilizado em recepções, o $h1 é a melhor opção para você. Não perca essa oportunidade de adquirir agora mesmo o seu, acesse!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/painel-eletronico-1.jpg" alt="Painel Eletrônico" title="Painel Eletrônico" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							A criação do <strong>outdoor eletrônico</strong> foi uma inovação que não apenas acelerou o processo de viabilização de um anúncio, mas também ajudou a diminuir a quantidade de poluentes que são disseminados sobre a superfície da terra.
							Isso acontecia porque todas as peças que seriam anunciadas em outdoors utilizavam um tipo de papel que era mais resistente do que os manuseados no dia a dia da maioria das pessoas, pois ele precisava suportar os mais diferentes tipos de adversidades, como chuva, sol e etc, isso tornava o processo de decomposição do material mais difícil.
							Além disso, os anúncios eram trocados com uma frequência muito grande, por essa razão, a produção de papel que seria jogado fora era muito grande, e nem metade de tudo o que era produzido passava pelo processo de reciclagem, gerando mais poluentes e prejudicando o planeta. O <strong>painel eletrônico para propaganda</strong> não apenas solucionou esse problema, como também trouxe uma série de vantagens.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Painel de LED</h2>
						<p>O <strong>painel eletrônico de LED</strong> é composto por diversas lâmpadas que se iluminam de modo a transmitir as mensagens desejadas, variando a quantidade de acordo com o tamanho de cada um.
							É importante informar para quem está desejando comprar um aparelho como esse, que a maioria as lâmpadas de LED podem ser expostas a chuva e sol intenso sem sofrer qualquer dano, mas isso não significa que os letreiros de LED também poderão.
						O bocal da lâmpada deve estar sempre protegido e isolado, e existem alguns equipamentos onde isso acontece. Se nesse caso, o produto for exposto às ações do tempo, é muito provável que o mesmo sofrerá um curto circuito e parará de funcionar.</p>
						<h2>Letreiro de LED</h2>
						<p>O <strong>letreiro eletrônico</strong> também pode ser utilizado em outros ambientes e com outras finalidades, mesmo sendo muito usado para chamar a atenção de possíveis clientes, ele também pode ser um produto informativo, que direciona a população em uma certa direção ou indica as horas, como acontece em algumas estações de trem e metrô, por exemplo.
						Essa multifuncionalidade proveniente do <strong>painel eletrônico de leds</strong> acontece por conta da personalização de cores, tamanhos e formatos que só um produto como esse consegue oferecer.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/painel-eletronico-2.jpg" alt="Painel Eletrônico" title="Painel Eletrônico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-eletronico.php');?>
<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>