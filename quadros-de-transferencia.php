<? $h1 = "Quadros de Transferência"; $title  = "Quadros de Transferência"; $desc = "Equipamento que auxilia no funcionamento de geradores, os quadros de transferência são essenciais em ambientes industriais. Acesse o site e faça seu orçamento!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Muitas empresas, independente do ramo de atuação das mesma, já sofreram com os problemas que a falta de energia de pode gerar. O colaborador está realizando uma atividade importante quando de repente acaba a luz e tudo que foi desenvolvido até o momento acaba se perdendo, não difícil encontrar alguém que já tenha vivido uma história essa.
							Com o objetivo de solucionar esse problema é que muitas instituições resolveram instalar geradores potentes em seus ambientes de trabalho, porém transferir um equipamento de uma fonte de energia para outra automaticamente não é uma tarefa simples, isso exige a instalação de um dispositivo especial chamado <strong>painel qta</strong>.
							Além de fazer a transferência de uma fonte para a outra, ele possui mais uma função que auxilia a empresa. Quando a fonte de luz exterior se estabiliza e a volta a existir o fornecimento de energia, o painel continuará funcionando por um tempo pré estabelecido, desta maneira a energia no local se mantém estável sem causar grandes problemas ou impactos, e o cliente não sofrerá com idas e vindas de luz.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						
						<h2>Chave de Transferência Automática</h2>
						<p>O <strong>Quadro QTA</strong> é um produto muito comercializado por empresas de energia. Como acontece com muitos equipamentos, ele possui uma divisão entre dois modelos para auxiliar os consumidores na hora de escolher o produto que mais se adequa às necessidades dele.
							O quadro QTA de funcionamento aberto é o primeiro modelo, ele é acionado por um controlador microprocessado e a transferência é feita por uma interrupção momentânea do fornecimento de energia, contendo componentes que garantirão a segurança do sistema.
						Existe ainda o quadro QTA em rampa, um modelo que conta com dois contatores e/ou disjuntores intertravados eletricamente, que são acionados por um controlador microprocessado que realiza a transferência e a re-transferência sem a interrupção do fornecimento de energia.</p>
						<h2>QTA para Gerador</h2>
						<p>O quadro de transferência automática deve ser implementado em sua empresa por um especialista no assunto e que acima de tudo, entenda o funcionamento do equipamento e conheça as instalações do cliente, só dessa forma será possível instalar um dispositivo 100% personalizado.
						Não perca tempo entrando em vários sites para conseguir realizar uma cotação, confie em um dos maiores portais voltados para a indústria e encontre os melhores <strong>gerador com QTA</strong> com um clique.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg" alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-mpi.php');?>
<button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar">Orçamento Grátis</button>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>