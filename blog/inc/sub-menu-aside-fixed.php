<?php 
    $blogCatUrl = rtrim(Check::CatByParent($itemParent, EMPRESA_CLIENTE), '/');
    $asideMenuTitle = Check::CatByUrl(end(explode('/', $blogCatUrl)), EMPRESA_CLIENTE);
?>

<h2><a href="<?= RAIZ.'/'.$blogCatUrl ?>" title="<?= $asideMenuTitle ?>">
<?= $asideMenuTitle ?></a></h2>
<nav itemscope itemtype="https://schema.org/SiteNavigationElement">
    <ul class="sub-menu">
        <?php $Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :stats AND cat_parent = :cat ORDER BY cat_title", "stats=2&cat={$itemParent}");
        if ($Read->getResult()): ?>
            <?php foreach ($Read->getResult() as $item): ?>
            <li>
                <a class="submenu-item submenu-item--last" href="<?= RAIZ.'/'.$blogCatUrl.'/'.$item['cat_name']; ?>" title="<?= $item['cat_title'] ?>"><?= $item['cat_title'] ?></a>
            </li>
            <?php endforeach; ?>
        <?php endif;

        $Read->ExeRead(TB_BLOG, "WHERE blog_status = :stats AND cat_parent = :cat ORDER BY blog_title", "stats=2&cat={$itemParent}");
        if ($Read->getResult()): ?>
            <?php foreach ($Read->getResult() as $item): ?>
            <li>
                <a class="submenu-item submenu-item--last" href="<?= RAIZ.'/'.$blogCatUrl.'/'.$item['blog_name']; ?>" title="<?= $item['blog_title'] ?>"><?= $item['blog_title'] ?></a>
            </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
</nav>