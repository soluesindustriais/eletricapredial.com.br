<? $h1 = "Fabricante de painel elétrico"; $title  = "Automação de quadros elétricos"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
		<div class="content">
			<section>
				<?=$caminhoquadro_eletrico?>
				<? include('inc/quadro-eletrico/quadro-eletrico-buscas-relacionadas.php');?>
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-6">
						<img src="<?=$url?>imagens/fundo-contato.jpg" alt="">
					</div>
					<div class="col-6">
						<h1>
						<?=$h1?>
						</h1>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias nemo neque sequi ipsam, vitae provident aspernatur quasi amet itaque facilis ullam sunt, fugit ab illo debitis eligendi totam, illum impedit.
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi aut, cum unde adipisci ab repudiandae itaque placeat, beatae hic fugit laboriosam vitae molestiae doloremque quisquam magni voluptatibus, inventore! Natus, ipsum! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi error assumenda quo, labore, sit provident iure quidem, possimus et delectus quae. Beatae dolores ipsa earum velit qui, assumenda nihil eveniet. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur sunt suscipit inventore blanditiis sequi, sapiente ipsa porro recusandae voluptatum corporis impedit explicabo ducimus quae, soluta, neque debitis autem hic numquam!
						</p>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<div class="wrapper">
					<br class="clear">
					<br class="clear">
					<br class="clear">
					
					<div class="col-6">
						<h2>
						titulo
						</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia cumque a perspiciatis unde pariatur, veniam aspernatur fugit blanditiis! Illo adipisci aut cumque, officia voluptas ipsum consequuntur odit temporibus quos quis.</p>
						<h2>
						titulo
						</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere non, beatae, distinctio earum nam rerum consectetur perferendis enim. Velit consequuntur repudiandae quo nisi possimus quam sapiente assumenda tenetur minima voluptates.</p>
						
					</div>
					<div class="col-6">
						<img src="<?=$url?>imagens/fundo-contato.jpg" alt="">
					</div>
				</div>
				<article>
					<p>
						<?=$desc?>
					</p>
					<br class="clear">
					<? include('inc/galeria-fixa-mpi.php');?>
					<br class="clear">
					<h2>
					
					Quadro de distribuição trifásico industrial
					</h2>
					<p>
						O produto pode servir como um <strong>quadro geral de distribuição </strong> ou de alimentação, além de <strong>quadro de comando</strong> ou automação de máquinas e equipamentos. Os quadros servem para distribuição e contenção dos circuitos em uma planta industrial, e são as centrais de operação e controle de uma instalação elétrica.
					</p>
					<p>
						A estrutura do quadro de comando pode ser de chapa de aço com tranca, isso vai depender do projeto de aplicação do mesmo. O produto também pode ser um box de material termoplástico, totalmente auto-extinguível. Estão disponíveis três tipos de porta para este modelo de quadro: porta opaca, porta transparente e porta reversível.
					</p>
					<p>
						
						O produto foi desenvolvido através do levantamento da carga da planta industrial, do setor ou do equipamento que precisa do mesmo. Este trabalho é feito pelo engenheiro elétrico, por isso é necessário o cliente escolher um bom profissional para desenvolver esse serviço.
					</p>
					<h3>
					Caixa de distribuição
					</h3>
					<p>
						No <strong> centro de distribuição </strong> terá circuitos sobressalentes, como solicitam as normas e padronizações para este trabalho. O quadro também deve ter placa de identificação, em local visível e com os dados fornecidos de maneira legível e com vida útil, já que são informações extremamente importantes.
					</p>
					<p>
						Os quadros devem seguir a norma NBR 5410, que determina segurança, composição, exposição e proteção do <strong>quadro de distribuição trifásico </strong> , entre outras diretrizes.
					</p>
					<h3>
					Quadro de disjuntores
					</h3>
					<p>
						O <strong> quadro de distribuição de energia</strong> é feito em chapas de aço, como citado anteriormente, de ótima qualidade, vida útil extensa e pintura eletrostática. Caso o cliente deseje outro tipo de acabamento, pode incluir no pedido alguns itens especiais, claro que vai depender do fabricante, se ela realiza ou não essas inclusões e/ou alterações. O produto pode ter diversos compartimentos que podem ser trancados ou não, garantindo extrema segurança e controle. Assim, o cliente pode determinar quais são as pessoas que têm acesso ao painel.
					</p>
					<p>
						Para melhor distribuição e eficiência no ambiente industrial, é imprescindível contar com o quadro elétrico industrial.
					</p>
					<hr />
					<!-- 					<?include('inc/tabela.php');?> -->
					<hr/>
					<? include('inc/produtos-relacionados-mpi.php');?>
				</article>
				<? include('inc/coluna-lateral-mpi.php');?>
				<br class="clear">
				<? include('inc/form-mpi.php');?>
				<? include('inc/regioes.php');?>
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>