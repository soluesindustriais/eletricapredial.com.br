<? $h1 = "Fabricante de painel elétrico"; $title  = "Painel Industrial | Elétrica Predial"; $desc = "Já parou para pensar como seria bom conseguir cotar equipamentos e serviços industriais em apenas um lugar? Na Elétrica Predial você pode, acesse e confira!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O <strong>painel de controle elétrico</strong> é uma instalação de um conjunto de operações e controles ligados em algum ponto de um sistema. O objetivo principal dessa instalação é de controlar e direcionar a energia elétrica para toda a rede de equipamentos envolvida. Para que isso ocorra de forma precisa, é importante contar com um fabricante de painel, pois somente os profissionais capacitados possuem a técnica necessária para realizar uma análise correta e estratégica para o melhor funcionamento do mesmo. Esses painéis elétricos podem ser utilizados em comércios, grandes indústrias, condomínios e residências, por exemplo.
							Por conta das suas inúmeras funções, que determinam o funcionamento de todos os outros equipamentos de um negócio, é preciso buscar uma distribuidora que realize a <strong>montagem de painéis elétricos</strong> com total confiança. Como dito anteriormente, esse painel pode ser instalado em diversos locais, e as principais aplicações dele se baseiam em eletrodomésticos, luzes, tomadas, máquinas e até em objetos de baixa e média instalação.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Montagem de painel elétrico</h2>
						<p>A montagem de um painel elétrico não é uma função tão fácil como parece, isso porque ela demanda o máximo de atenção do eletricista. Esta atenção é extremamente importante, pois o <strong>painel de comando</strong> irá trabalhar sozinho ou de forma manual contínua, e assim deve-se prestar atenção não somente na funcionalidade do que foi programado, mas também no design e segurança.
						De um modo amplo sobre o assunto, o <strong>painel elétrico</strong> possui alguns componentes que estarão presentes em todos os quadros e alguns deles são mais específicos. Esses quadros normalmente possuem condutores, relés, alarmes, sinaleiros, bornes, chave comutadora e também o Soft Starter.</p>
						<h2>Quadro de distribuição de força</h2>
						<p>O quadro elétrico industrial abriga um ou mais sistemas de segurança e/ou manobra, e a conexão de condutores elétricos para que realize a distribuição da energia elétrica satisfatoriamente. Esse tipo de equipamento é como se fosse o cérebro de todo o sistema que precisa da energia elétrica para serem alimentados, como os circuitos de tomadas, circuitos de iluminação e circuito de emergências.
						Para melhor distribuição e eficiência no ambiente industrial, é imprescindível contar com o <strong>quadro elétrico industrial</strong>.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg" alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-eletrico.php');?>
<button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar">Orçamento Grátis</button>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>