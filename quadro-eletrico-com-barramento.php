<? $h1 = "Empresas para painéis elétricos"; $title  = "Quadro de Distribuição com Barramento | Elétrica Predial"; $desc = "A Elétrica Predial é um portal voltado para a indústria onde empresas do mundo inteiro se conectam para realizar negócio, faça parte você também!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="quadro-eletrico" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O <strong>quadro de distribuição de energia</strong> organiza a conectividade da energia elétrica do ambiente inserido, seja ele industrial, residencial, comercial, entre outras aplicações. No que se refere à sua composição estrutural, é feito de chapas de aço de excelente qualidade, com durabilidade extensa e pintura eletrostática. O tamanho do produto varia de acordo com o projeto do painel.
							O <strong>barramento para quadro de distribuição</strong> é um objeto de cobre eletrolítico. O diferencial do produto é que muitos problemas são identificados somente no barramento, pois a falta de manutenção ou a má montagem dos quadros trazem transtornos a toda instalação elétrica. O mesmo é fabricado conforme as normas de segurança estabelecidas, entre elas: NBR 5410, NBR 6524 e NBR 5111.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Centro de distribuição</h2>
						<p>O <strong>quadro de distribuição geral</strong> pode ser utilizado para controlar a energia elétrica que mantém um prédio, indústria, hospitais e outros espaços. Porém, o produto também pode ser projetado para fazer a distribuição compartimentada e segmentada de energia elétrica, por isso o produto é muito importante na rotina de qualquer colaborador e/ou empreendedor.
						Para fazer os painéis é preciso contar com diversos componentes elétricos como temporizadores, botoeiras, disjuntores, chaves reversoras, contatoras, relés, entre outros objetos. Todos os componentes são organizados de maneira eficaz dentro do quadro para possibilitar a distribuição de energia elétrica de forma segura e eficiente a qualquer ambiente.</p>
						<h2>Quadro elétrico industrial </h2>
						<p>O quadro industrial pode ser feito um <strong>painel de distribuição</strong>, que tem o objetivo de garantir o fornecimento de energia elétrica para todos os ambientes da indústria. Pode ser projetado um quadro de automação e comando, para controlar motores e comandar CLP’s, IHM’s e Supervisórios, com softwares específicos para a orientação das funções de máquinas em uma mesa de comando. Esses quadros são desenhados, montados e instalados conforme o orçamento que o cliente fez para o projeto, sempre identificando as necessidades do mesmo.
						A função deste produto não é diferente das demais, já que deve distribuir energia elétrica para todos os circuitos de uma unidade industrial, acionar motores ou comandar equipamentos. Os quadros são feitos com disjuntor geral e mini disjuntores, barramentos, IDR/DDR e muitos outros componentes. O painel obedece à planta da fábrica e pode ser monofásico, bifásico e trifásico.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/quadro-eletrico-comprar.jpg" alt="quadroeletrico-comprar" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-quadro-eletrico.php');?>

<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>