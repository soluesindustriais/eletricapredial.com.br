<? $h1 = "Painéis para transferência automática"; $title  = "Chave Reversora para Gerador | Elétrica Predial"; $desc = "O QTA é um equipamento indicado para empresas que possuem geradores, na Elétrica Predial você orça esse e outros dispositivos com um só clique. Acesse!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/quadro-de-distribuicao-eletrica.jpg" alt="Quadro de Distribuição Elétrica" title="Quadro de Distribuição Elétrica" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O painel de transferência automática é conhecido no âmbito industrial como <strong>QTA</strong>, ou <strong>quadro de transferência automático</strong>, consiste em um painel elétrico de controle que pode ser aplicado em <strong>geradores de energia</strong> ou qualquer sistema elétrico dentro de uma empresa, com a missão de acionar, sem interferência manual, a partida dos equipamentos logo após a interrupção ou queda de energia. Ou seja, em caso de falta de energia, o gerador começa a funcionar automaticamente suprindo a energia interrompida.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>QTA para geradores</h2>
						<p>Os <strong>geradores</strong> são uma fonte de energia para situações de emergência. Normalmente os equipamentos são utilizados em locais onde apresentam uma grande necessidade de energia elétrica para inúmeras atividades, como empresas e indústrias. E o QTA para geradores é um equipamento importante para que ocorra a transferência de forma rápida e fácil, com um possível intervalo de 15 segundos para que operação ocorra.
						Normalmente, o quadro é fabricado com materiais certificados e da melhor qualidade, seguindo os padrões de segurança para assim ofertarem a melhor solução que irá atender as necessidades do sistema de gerador elétrico.</p>
						<h2>Chave reversora</h2>
						<p>Outro equipamento utilizado nos geradores é a chave reversora, também conhecida como <strong>chave comutadora</strong>, sua principal missão é alternar a fonte de alimentação da rede concessionária para os geradores. Os equipamentos que geram energia, portanto, confirmam seu papel de solução eficiente e segura, alternando por via da chave, em qualquer situação, a comutação entre a rede da concessionária e o grupo gerador.
						O equipamento é imprescindível em qualquer instalação que utilize o gerador como fonte alternativa de energia, ou melhor, o dispositivo só é desnecessário quando é o próprio grupo gerador a única fonte de energia. Em casos de hospitais, por exemplo, onde a alimentação de energia deve ser constante e ininterrupta por conta de aparelhos e centros cirúrgicos, ela é indispensável para assegurar o fornecimento ininterrupto de energia.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/quadro-de-distribuicao-comprar.jpg" alt="Quadro de Distribuição" title="Quadro de Distribuição">
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-transferência.php');?>

<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>