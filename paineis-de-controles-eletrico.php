<? $h1 = "Painéis de Controle Elétrico"; $title  = "Painéis de Controle Elétrico"; $desc = "Solicite um orçamento para painéis de controle elétrico na Elétrica Predial e aproveite para ficar por dentro das principais novidades do segmento. Confira!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O <strong>painel de controle</strong> elétrico, é um equipamento usado nas mais diversas parcelas do mercado industrial. Justamente como forma de atender a essa demanda, é que esse aparelho possui a praticidade de poder ser modificado, de modo que ele se adapte melhor a máquina onde será conectado.
							É importante ressaltar que, independente do ambiente onde esse produto será adaptado, a <strong>montagem de painéis elétricos</strong> deverá sempre ser realizada utilizando uma matéria prima resistente, capaz de resistir a esforços mecânicos, elétricos e térmicos, bem como aos efeitos da umidade, comum na maioria dos ambientes industriais.
							Além disso, os dispositivos e circuitos que irão compor o equipamento deverão ser dispostos de maneira que facilite sua operação e manutenção e, ao mesmo tempo, assegure o grau necessário de segurança.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Painel de Comando Elétrico</h2>
						<p>Como dito anteriormente, os <strong>painéis elétricos</strong> são usados nos mais diversos locais, por essa razão ele pode ser visto com as mais diversas formas construtivas, tudo vai depender da aplicação, do local, do espaço disponível e da mobilidade.
						Dentre os principais modelos, existem alguns que podemos destacar, são eles:</p>
						<ul>
							<li>Painel elétrico armário;</li>
							<li>Painel elétrico multicolunas;</li>
							<li>Painel elétrico de comando.</li>
						</ul>
						<h2>Quadro de Comando Preço</h2>
						<p>Desenvolver um <strong>centro de controle</strong> em uma empresa, exige que a pessoa realize uma busca detalhada sobre todas as empresas que fornecem ou fabricam esse tipo de mercadoria, porém, nem sempre é possível perder algum tempo realizando uma atividade tão cansativa como essa.
							Por essa razão é que existem portais diferenciados, eles são especializados em atender as demandas do segmento industrial e com certeza poderão lhe indicar o melhor fornecedor para seus clientes.
						Nesses marketplaces o consumidor poderá conhecer todas as empresas que desenvolvem o produto desejado e entrar em contato com a mesma para solicitar uma cotação, tal comunicação permite que o usuário adquira mais confiança em seu possível provedor e se sinta mais confortável ao fechar a compra.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg" alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-eletrico.php');?>
<button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar">Orçamento Grátis</button>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>