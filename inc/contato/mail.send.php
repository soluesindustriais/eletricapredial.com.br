<?php

// CONFIG do site (geral.php)
$EMPRESA = $nomeSite;
$EMAIL = "naoresponder@eletricapredial.com.br"; // E-MAIL QUE MANDAR A MENSAGEM

// Classe 
require("PHPMailer/PHPMailerAutoload.php");

//Setando as configurações de envio de email, com autenticação do smtp Locaweb
$mail = new PHPMailer();
$mail->IsSMTP();
$mail->From = $EMAIL;
$mail->FromName = $EMPRESA;
$mail->Host = "email-ssl.com.br";
$mail->Port = '587';
$mail->SMTPAuth = true;
$mail->Username = $EMAIL;
$mail->Password = $senhaEmailEnvia;
$mail->isHTML();
$mail->CharSet = 'utf-8';
// $mail->SMTPSecure = 'ssl';
$mail->AltBody = 'Para ver este e-mail corretamente ative a visualização de HTML';
$mail->SMTPOptions = array(
  'ssl' => array(
    'verify_peer' => false,
    'verify_peer_name' => false,
    'allow_self_signed' => true
  )
);