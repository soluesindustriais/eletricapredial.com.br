
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>

<script class="lazyload" src="js/jquery.hoverdir.js"></script>
<script>
    $(function() {
        $(' .thumbnails-mod17 > li ').each(function() {
            $(this).hoverdir({
                hoverDelay: 75
            });
        });
    });
</script>
<?php $lista = array('Comprar quadro de transferência manual', 'Painel de transferência', 'Painel de transferência automática', 'Painel de transferência automática para geradores', 'Painel de transferência para gerador', 'Qta para gerador', 'Qta quadro de transferência automática', 'Quadro de comando', 'Quadro de disjuntores', 'Quadro de distribuição', 'Quadro de distribuição de energia', 'Quadro de distribuição de força', 'Quadro de distribuição elétrica', 'Quadro de energia montado', 'Quadro de transferência', 'Quadro de transferência automática', 'Quadro de transferência automática gerador', 'Quadro de transferência automática para gerador', 'Quadro de transferência automática preço', 'Quadro de transferência automático para geradores', 'Quadro de transferência manual', 'Quadro de transferência manual para gerador', 'Quadro distribuição', 'Quadro elétrico externo', 'Quadro elétrico montado', 'Quadro eletrônico', 'Quadro monofásico', 'Quadro transferência automática gerador', 'Quadro trifásico', 'Instalação de quadro de comando', 'Quadro de comando automação');
                                shuffle($lista);
                                for ($i = 1; $i < 13; $i++) { ?>
<?php
$folder = "quadro-de-transferencia";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/quadro-de-transferencia/quadro-de-transferencia-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-06"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/quadro-de-transferencia/quadro-de-transferencia-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/quadro-de-transferencia/thumbs/quadro-de-transferencia-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>