
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script  src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Automação painel elétrico','Climatizador de painel elétrico','Comprar painéis elétricos','Condicionador de ar para painel elétrico','Conectores para painel elétrico','Empresa de condicionador de ar para painel elétrico','Empresa de montagem de painéis elétricos sp','Empresa montadora de painel elétrico','Empresas de montagens de painéis elétricos','Empresas de painéis elétricos','Empresas de painéis elétricos em sp','Empresas montadoras de painéis elétricos em sp','Empresas montadoras de painéis elétricos industriais','Fabricante de condicionador de ar para painel elétrico','Fabricante de painel elétrico','Fabricantes de painéis elétricos sp','Frequencímetro para painel elétrico','Gabinete para painel elétrico','Manutenção de painéis elétricos','Manutenção em painéis elétricos industriais','Manutenção preventiva em painéis elétricos','Montador de painéis elétricos','Montadoras de painéis elétricos','Montagem de painéis elétricos','Montagem de painéis elétricos industriais','Montagem de painéis elétricos sp','Montagem de painel de comando elétrico','Montagem de painel elétrico residencial','Montagem painel elétrico','Montar painel elétrico residência','Painéis elétricos conforme nr10','Painéis elétricos de baixa e média tensão','Painéis elétricos de baixa tensão','Painel comando elétrico','Painel de comando elétrico','Painel de comando elétrico para queimadores','Painel de comando elétrico preço','Painel de controle elétrico','Painel elétrico','Painel elétrico a prova de explosão','Painel elétrico autoportante','Painel elétrico bifásico','Painel elétrico ccm','Painel elétrico clp','Painel elétrico com barramento','Painel elétrico com porta interna','Painel elétrico com tampa de acrílico','Painel elétrico de bomba','Painel elétrico de comando','Painel elétrico de iluminação','Painel elétrico de inox','Painel elétrico de plástico','Painel elétrico de policarbonato','Painel elétrico em aço inox','Painel elétrico em fibra de vidro','Painel elétrico em inox','Painel elétrico em pvc','Painel elétrico estrela triangulo','Painel elétrico grande','Painel elétrico industrial','Painel elétrico industrial preço','Painel elétrico industrial usado','Painel elétrico metálico','Painel elétrico monofásico','Painel elétrico montado','Painel elétrico náutico','Painel elétrico ncm','Painel elétrico nr10','Painel elétrico obstruído','Painel elétrico para barcos','Painel elétrico para câmara fria','Painel elétrico para compressor','Painel elétrico para lancha','Painel elétrico para ponte rolante','Painel elétrico preço','Painel elétrico predial','Painel elétrico residencial','Painel elétrico residencial trifásico','Painel elétrico termoplástico','Painel elétrico tipo armário','Painel elétrico tipo robô','Painel elétrico trifásico','Painel elétrico usado','Painel elétrico valor','Painel modular elétrico','Preço montagem painel elétrico','Reforma de painéis elétricos','Reforma e montagem de painéis elétricos','Refrigerador de ar para painel elétrico','Resfriadores de painéis elétricos','Serviço de projeto e montagem de painéis elétricos','Serviços de montagem de painéis elétricos','Sistemas de incêndio para painéis elétricos','Valor de painel elétrico','Venda de painel elétrico'); shuffle($lista); for($i=1;$i<13;$i++){ ?>
<?php
$folder = "painel-eletrico";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/painel-eletrico/painel-eletrico-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-06"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/painel-eletrico/painel-eletrico-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/painel-eletrico/thumbs/painel-eletrico-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>