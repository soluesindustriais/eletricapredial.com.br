<section class="banner-index wrapper-index">
    <div class="txt-side">
        <h1>Bem-vindo ao <span>Elétrica predial</span></h1>
        <div class="text-side-button">
            <a href="<?php echo $url ?>produtos-categoria" title="Página de produtos">Produtos</a>
            <a href="<?php echo $url ?>sobre-nos" title="Mais informações sobre a empresa">Informações</a>
        </div>
    </div>
    
    <div class="slide-side">
        <div class="slide-img slide-img-active">
            <a href="<?php echo $url ?>sobre-nos" title="Página sobre nós">
                <img src="<?php echo $url ?>imagens/banner.jpg" alt="imagem de uma Cesta básica preço">
            </a>
        </div>
       
    </div>
</section>