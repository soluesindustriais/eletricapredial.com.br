
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script  src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Automação industrial painel de controle','Condicionador de ar para painéis','Conector circular para painel','Conector de painel','Conector para painel multivia','Conversor de sinais para fundo de painel','Empresas de montagem de painéis industriais','Frequencímetro para painel','Indicador digital para painel','Instalação de painel de comando','Máquina de painel de corte','Montagem de painéis ccm','Montagem de painéis de comando','Montagem de painéis tta','Montagem de painel','Montagem de painel de automação','Montagem painel de comando industrial','Painéis acústicos industriais','Painéis com inversor','Painéis e unidades hidráulicas','Painéis inversores','Painel acústico preço','Painel automação','Painel automático qta','Painel barreira acústica','Painel canaletado preço','Painel clp','Painel comando','Painel contador de dias','Painel cronômetro com alarme','Painel de acesso','Painel de automação','Painel de automação de irrigação','Painel de automação industrial','Painel de automação para gerador','Painel de automação para irrigação','Painel de baixa tensão','Painel de chamada','Painel de comando','Painel de comando para elevadores','Painel de comando para motores','Painel de configuração','Painel de controle','Painel de controle de pintura','Painel de controle empresarial','Painel de controle ihm','Painel de controle para extinção','Painel de controle para indústria','Painel de controle plc','Painel de dias sem acidentes','Painel de distribuição elétrica','Painel de distribuição elétrica industrial','Painel de eletricidade','Painel de energia elétrica','Painel de força','Painel de ihm','Painel de led','Painel de mensagem led luminoso','Painel de mensagem para viatura','Painel de mensagem variável','Painel de mensagem variável móvel','Painel de mensagens','Painel de mensagens variadas','Painel de pc','Painel de secagem infravermelho','Painel de segurança','Painel endereçável','Painel equipamentos elétricos','Painel fotovoltaico','Painel indicador de preços','Painel indicador de produção','Painel industrial','Painel informativo','Painel informativo de produção','Painel inteligente de alarme de incêndio','Painel led','Painel modular termkcal','Painel organizador','Painel para câmara frigorífica','Painel para máquina de solda','Painel para secagem infravermelho','Painel para tv com led','Painel para tv led','Painel plc','Painel porta fichas','Painel relógio com cronômetro','Painel solar fotovoltaico preço','Painel termo isolante','Painel tta','Painel wall','Painel wall para estrutura metálica','Painel wall para mezaninos','Serviço de montagem de painel industrial','Transformadores para painéis'); shuffle($lista); for($i=1;$i<13;$i++){ ?>
<?php
$folder = "painel";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/painel/painel-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-06"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/painel/painel-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/painel/thumbs/painel-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>