<? $h1 = "Quadro de distribuição trifásico industrial"; $title  = "Quadro de distribuição trifásico industrial"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O produto pode servir como um <strong>quadro geral de distribuição</strong> ou de alimentação, além de <strong>quadro de comando</strong> ou automação de máquinas e equipamentos. Os quadros servem para distribuição e contenção dos circuitos em uma planta industrial, e são as centrais de operação e controle de uma instalação elétrica.
							A estrutura do quadro de comando pode ser de chapa de aço com tranca, isso vai depender do projeto de aplicação do mesmo. O produto também pode ser um box de material termoplástico, totalmente auto-extinguível. Estão disponíveis três tipos de porta para este modelo de quadro: porta opaca, porta transparente e porta reversível.
							O produto foi desenvolvido através do levantamento da carga da planta industrial, do setor ou do equipamento que precisa do mesmo. Este trabalho é feito pelo engenheiro elétrico, por isso é necessário o cliente escolher um bom profissional para desenvolver esse serviço.
						</p>
						
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6">
						<h2>Caixa de distribuição </h2>
						<p>No <strong>centro de distribuição</strong> terá circuitos sobressalentes, como solicitam as normas e padronizações para este trabalho. O quadro também deve ter placa de identificação, em local visível e com os dados fornecidos de maneira legível e com vida útil, já que são informações extremamente importantes.
						Os quadros devem seguir a norma NBR 5410, que determina segurança, composição, exposição e proteção do <strong>quadro de distribuição trifásico</strong>, entre outras diretrizes.</p>
						<h2>Quadro de disjuntores</h2>
						<p>O <strong>quadro de distribuição de energia</strong> é feito em chapas de aço, como citado anteriormente, de ótima qualidade, vida útil extensa e pintura eletrostática. Caso o cliente deseje outro tipo de acabamento, pode incluir no pedido alguns itens especiais, claro que vai depender do fabricante, se ela realiza ou não essas inclusões e/ou alterações. O produto pode ter diversos compartimentos que podem ser trancados ou não, garantindo extrema segurança e controle. Assim, o cliente pode determinar quais são as pessoas que têm acesso ao painel.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<span class=" btn-produto" >PDF </span>
				<hr>
				<div class="wrapper-fixa">
					<p>
						<?=$desc?>
					</p>
					<? include('inc/galeria-fixa-mpi.php');?>
					<br class="clear">
				</div>
				<br class="clear">
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>