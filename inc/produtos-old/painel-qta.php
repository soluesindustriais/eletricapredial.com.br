<? $h1 = "Painel QTA"; $title  = "Painel QTA"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Quando a energia elétrica de qualquer tipo de produção industrial cai, o local perde o seu sistema produtivo, já que deixa de funcionar as máquinas, mesmo que seja temporário. Caso ocorra as falhas momentâneas, as indústrias contam com sistemas de geradores, para assim não ter nenhuma interferência na produção, garantindo que a energia elétrica dê continuidade às tarefas desenvolvidas pelos setores.
							Os geradores necessitam ser acionados nos momentos que ocorrem as interrupções de fonte de energia regular, nestes casos, entra a função do <strong>painel automático QTA</strong>. O produto é o principal elemento no sistema de energia, pois tem a responsabilidade de comandar o gerador de eletricidade, fazendo ele ligar ou ser desligado, sempre nos momentos certos para fornecer energia capaz de continuar as máquinas, sejam elas de indústrias, prédios comerciais, fazendas de agricultura, até mesmo residenciais.
						</p>
						
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6">
						<h2>Painel de quadros</h2>
						<p>Automatizando o aparelho para ser ligado quando necessário, o <strong>painel de automação</strong> possui configuração para trabalhar de modo mais adequado com o sistema elétrico por geradores, realizando as funções essenciais como a comutação imediata ou então a interrupção do sinal elétrico que vem da rede externa, da concessionária de energia. Um equipamento extremamente útil para os dias atuais, já que muitas empresas dependem de máquinas para produção, que seja de objetos, como por exemplo fabricantes de garrafas pet, até mesmo nos ambientes comerciais com o uso de computadores e impressoras.</p>
						<h2>Tipos de geradores elétricos</h2>
						<p>A diferença do <strong>painel de comando elétrico</strong> para outros painéis, está na sua possibilidade de determinar funções e comandos controlados pelo operador através de botões e dispositivos de acionamento. Esse tipo de painel é sugerido para os casos de acionamento de bombas de recalque, bombas de incêndio e outras. Mas existem várias outras situações, como os painéis para partida com soft starter, para partida com inversor de frequência, com utilização de CLP, entre outros.
						A escolha do tipo de painel e <strong>quadro elétrico</strong> vai depender completamente da complexidade do projeto e da utilização que será feita do equipamento. Alinhando o estudo de cada caso com engenheiros especialistas da obra, é fácil encontrar o equipamento correto e ideal que irá suprir as necessidades dos clientes.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<span class=" btn-produto" >PDF </span>
				<hr>
				<div class="wrapper-fixa">
					<p>
						<?=$desc?>
					</p>
					<? include('inc/galeria-fixa-mpi.php');?>
					<br class="clear">
				</div>
				<br class="clear">
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>