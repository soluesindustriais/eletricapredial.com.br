<? $h1 = "Painel de energia elétrica"; $title  = "Painel de energia elétrica"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
						A energia elétrica é a principal fonte de energia do mundo, ao redor do planeta, a principal forma de gerá-la é através das usinas hidrelétricas, porém sua produção também pode ser feita nas usinas eólicas, solares, termoelétricas, nucleares, etc.
						Ao sair das usinas elétricas e chegar até a residência ou empresa da maioria das pessoas, essa carga por uma série de transformadores que irão ajustar a sua tensão, até que a mesma esteja de acordo com a desenhada no <strong>projeto elétrico</strong>.
						</p>
						<p>Ao chegar em seu destino, a energia elétrica é encaminhada para o <strong>painel de distribuição</strong> e em seguida distribuída para cada tomada ou interruptor com a tensão pré designada para a mesma, isso quer dizer que as tomadas de 220V deverão receber essa tensão, o mesmo acontecerá com as tomadas de outros voltagens, como 110V, 380V e 440V. Caso a tensão errada chegue a uma determinada tomada, por exemplo, a mesma sofrerá um curto e parará de funcionar.</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6">
						h2>Caixa de Distribuição</h2>
						<p>Como a finalidade e o uso dos dois equipamentos são muito semelhantes, é comum que muitas pessoas não saibam a diferença entre o <strong>painel elétrico</strong> e um <strong>quadro de luz</strong>.
						Ninguém pode negar que os dois realizam atividades muito parecidas, mas a principal distinção não está em seu funcionamento e sim no local onde ele é encontrado, enquanto os painéis são mais utilizados no meio industrial, os quadros e caixas são mais comuns em residências e pequenos comércios.
						Por essa razão o primeiro possui um porte muito maior, além de ser equipado com os mais diversos tipos de componentes, como painéis CLP, sensores, medidores, inversores, contatores e muitos outros dispositivos.</p>
						<h2>Painel de Comando</h2>
						<p>Instalar um painel de comando é um benefício para todas as empresas, não apenas aquelas que atuam no segmento industrial. Grandes escritórios costumam possuir mesas enormes, todas repletas de computadores, celulares, notebooks e muitos outros equipamentos de informática conectados à tomada.
						Em recintos como esse, o <strong>painel de controle elétrico</strong> atua na gestão de energia, designando a quantidade necessária para que cada máquina funcione da melhor forma possível, sem exceder os gastos com energia elétrica ou manutenção nas instalações.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<span class=" btn-produto" >PDF </span>
				<hr>
				<div class="wrapper-fixa">
					<p>
						<?=$desc?>
					</p>
					<? include('inc/galeria-fixa-mpi.php');?>
					<br class="clear">
				</div>
				<br class="clear">
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>