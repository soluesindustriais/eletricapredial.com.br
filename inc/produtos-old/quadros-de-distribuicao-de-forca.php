<? $h1 = "Quadro de distribuição de força"; $title  = "Quadro de distribuição de força"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O <strong>quadro de distribuição</strong> tem como principal função receber e distribuir a energia elétrica, contida em um ou mais circuitos, dependendo do projeto em que está sendo estabelecido.
							Além disso, esse quadro pode ter dispositivos de proteção, dispositivos de manobra e condutores elétricos que são interligados, distribuindo a energia elétrica para todos os outros circuitos.
						</p>
						
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6">
						<h2>Quadro de distribuição elétrica </h2>
						<p>O <strong>quadro de força</strong> ou quadro de distribuição elétrica é um equipamento destinado a receber a energia de uma ou mais fontes de alimentação e distribuí-las a um ou mais circuitos. Esse equipamento pode abrigar um ou mais dispositivos de proteção e manobra, juntamente com a conexão de condutores elétricos interligados a eles, com propósito de distribuir a energia para esses circuitos.
						Em qualquer tipo de instalação elétrica, é importante saber como realizar os procedimentos correspondentes à execução de um projeto elaborado previamente, em conformidade com as especificações previstas na NBR 5410 (norma que regulamenta as instalações elétricas em baixa tensão) e NR-10 (segurança em instalações e serviços em eletricidade).</p>
						<h2>Quadro de disjuntores</h2>
						<p>Para que o <strong>quadro elétrico</strong> funcione corretamente, é necessário contar com os disjuntores. Os mesmos servem para proteger os circuitos que alimentam as cargas em todo e qualquer ambiente, podendo ser comercial ou residencial. Existem dois barramentos que possuem os condutores neutro e de proteção, onde o primeiro deve estar isolado eletricamente do quadro de distribuição e o segundo deve estar acoplado a ele, constituindo portanto a proteção dos circuitos contra choques no contato indevido com superfícies conduzindo energia, sendo que este encontra-se ligado ao aterramento geral da instalação.
						No caminho do <strong>quadro de distribuição de energia</strong> até os interruptores e tomadas, essa energia passa pelo quadro de medição que está associado a um equipamento, o qual mede o consumo mensal e daí então chega através de um ramal de entrada ao chamado <strong>quadro de distribuição preço</strong> justo e acessível, de onde partirão os circuitos que irão alimentar pontos de luz ou lâmpadas, interruptores para acionamento das lâmpadas, conhecidos como comandos, tomadas que fornecerão energia aos aparelhos eletroeletrônicos a elas plugados, além de cargas cuja potência é considerada elevada como chuveiros elétricos, máquinas de lavar, forno micro-ondas, entre muitos outros.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<span class=" btn-produto" >PDF </span>
				<hr>
				<div class="wrapper-fixa">
					<p>
						<?=$desc?>
					</p>
					<? include('inc/galeria-fixa-mpi.php');?>
					<br class="clear">
				</div>
				<br class="clear">
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>