<? $h1 = "Painel de controle elétrico"; $title  = "Painel de controle elétrico"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							<p>Um painel de controle elétrico, é um equipamento usado nos mais diversos segmentos industriais e que teve sua funcionalidade e tamanho adaptado para se adequar ao ambiente, nesses locais, ele é conhecido como <strong>quadro de luz</strong>.
								Além dos ajustes citados anteriormente, poucas são as características que diferenciam um equipamento do outro, já que os dois armazenam se forma segura e organizada todos os componentes responsáveis pelo fornecimento de energia elétrica de um local.
							</p>
							<p>Em um <strong>quadro de força</strong> é comum encontrar dispositivos como o disjuntores, ele foi desenvolvido para proteger as instalações elétricas, em caso de pane ou algum defeito, ele receberá todo o impacto não permitindo que os demais utensílios conectados a ele sejam afetas.</p>
							<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
							<br class="clear">
						</div>
					</div>
					<br class="clear">
					<br class="clear">
					<div  class="mpi-produtos-2">
						<br class="clear">
						<div class="col-1"><br></div>
						<div class="col-6">
							<h2>Caixa de passagem elétrica</h2>
							<p><p>Só quem já realizou um projeto elétrico, sabe como ele pode ser prático e benéfico para o funcionamento dos equipamentos, entre os diversos itens necessários para realizar um procedimento como esse, está a caixa de passagem, ela é a responsável por ligar todos os fios e cabos de uma tomada ou interruptor, até o <strong>quadro elétrico</strong>.
							O emprego da caixa de passagem facilita a manutenção e instalação do sistema elétrico, na medida em que a mesma é capaz de centralizar vários cabos em um único local, algo útil para a organização e distribuição da fiação.
							Outra vantagem desse equipamento, é que ele ainda pode ser utilizada em sistemas de telefonia e dados para passar, emendar ou terminar linhas nestas redes.
							</p>
							<p>Geralmente instaladas no início da obra, as caixas de passagem são instaladas ainda no início da obra, pois como os componentes desta peça são pouco seguro, é recomendado que ela fique embutida na parede e em um ambiente mais escondido, mesmo existindo alguns modelos diferentes.</p>
							<h2>Caixa de energia</h2>
							<p>A implementação de um <strong>centro de controle</strong> pode exigir um determinado tempo da empresa que deseja fazer uso desse método, mas a redução nos custos com energia elétrica e a tranquilidade de saber que sua instalação elétrica está segura, será recompensador.
							Tais vantagens só serão obtidas com o auxílio da <strong>caixa de disjuntores</strong>, ela será a responsável por armazenar os mais variados tipos de componentes, que juntos, formaram toda a instalação elétrica de uma casa, comércio ou fábrica.</p>
						</div>
						<div class="col-4">
							<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
						</div>
						<div class="col-1"><br></div>
					</div>
					<hr>
					<div  data-anime="in">
						<?include('inc/tabela.php');?>
					</div>
					<span class=" btn-produto" >PDF </span>
					<hr>
					<div class="wrapper-fixa">
						<p>
							<?=$desc?>
						</p>
						<? include('inc/galeria-fixa-mpi.php');?>
						<br class="clear">
					</div>
					<br class="clear">
					<? include('inc/form-mpi.php');?>
					
				</section>
			</div>
		</main>
	</div>
	<!-- .wrapper -->
	<? include('inc/footer.php');?>
</body>
</html>