<? $h1 = "Quadro elétrico trifásico"; $title  = "Quadro elétrico trifásico"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O quadro elétrico trifásico serve para a contenção e distribuição dos circuitos em uma planta.
							A utilização do <strong>quadro de distribuição trifásico industrial</strong> é de muita importância, pois ele opera em conjunto com o relógio de luz que necessita da energia elétrica, que chega por via da rede de distribuição que está na rua assim abastecendo todos os pontos de ligação que estão na fábrica, como tomadas de uso específico, tomadas de uso geral, bomba, iluminação, máquinas e outros circuitos.
						</p>
						<p>Por conta das suas inúmeras funções, que determinam o funcionamento de todos os outros equipamentos de um negócio, é preciso buscar uma distribuidora que realize a <strong>montagem de painéis elétricos industriais</strong> com total confiança. Como dito anteriormente, esse painel pode ser instalado em diversos locais, e as principais aplicações dele se baseiam em eletrodomésticos, luzes, tomadas, máquinas e até em objetos de baixa e média instalação.</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6">
						<h2>Quadro de força</h2>
						<p>Qualquer instalação elétrica necessita da utilização do quadro de força, pois a partir dele é possível abrigar os dispositivos de segurança e controle dos <strong>circuitos elétricos</strong>. Além de que, o quadro é responsável por distribuir toda a energia corrente que circula pelo local.
							A <strong>instalação do quadro elétrico</strong> deve ser realizada por profissionais e empresas competentes, capazes de certificar a segurança do local, e apresentarem um serviço de qualidade. O procedimento de instalação deve seguir as especificações previstas na NBR 5410 (norma que regulamenta as instalações elétricas em baixa tensão) e NR-10 (segurança em instalações e serviços em eletricidade).
						A pessoa que realizar a instalação do quadro elétrico, deve possuir conhecimentos em dimensionamento de instalações elétricas, interligação de componentes eletroeletrônicos e configuração e programação de componentes automatizados. Possibilitando assim que o circuito instalado no quadro opere da melhor forma possível, abastecendo com energia elétrica todos os pontos de distribuição alocados no local. </p>
						<h2>Circuitos elétricos</h2>
						<p>Os circuitos elétricos estão presentes dentro da aparelhagem elétrica. Um circuito elétrico é a junção de elementos elétricos, tais como resistores, capacitores, diodos, indutores, fontes de tensão, fontes de corrente, linhas de transmissão e interruptores, de modo que produzam um caminho fechado para a circulação da corrente elétrica.
						Por via dos equipamentos que compõe o circuito é possível controlar a energia elétrica que vem da rede de distribuição que passa pelo quadro de medição até chegar ao quadro de distribuição de energia, que deve estar instalado em um local seguro, longe do alcance de crianças.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<span class=" btn-produto" >PDF </span>
				<hr>
				<div class="wrapper-fixa">
					<p>
						<?=$desc?>
					</p>
					<? include('inc/galeria-fixa-mpi.php');?>
					<br class="clear">
				</div>
				<br class="clear">
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>