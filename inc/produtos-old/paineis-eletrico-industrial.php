<? $h1 = "Painéis elétricos industriais"; $title  = "Automação de quadros elétricos"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Os painéis elétricos industriais são equipamentos responsáveis por simplificar, verificar e administrar a energia que vem da fonte de alimentação de uma instalação industrial. Esse equipamento necessita ser montado de acordo com estudos e análises de toda a aparelhagem.
							Um <strong>painel elétrico</strong> é nada mais, nada menos que um compartimento modular utilizado para alocar dispositivos eletrônicos em seu interior. Normalmente, os painéis são elaborados em estruturas metálicas, com perfis de dobras perfurados ou não, apresentando fechamentos em chapas e portas com sistema de fecho.
							<p>
								O <strong>painel elétrico industrial</strong> deve ser fabricado com materiais capazes de suportar a esforços mecânicos, elétricos e térmicos. Os materiais apropriados auxiliam na proteção contra corrosão e ações climáticas, porém não é aconselhado deixar o equipamento exposto a chuvas e tempestades, para que não ocorra acidentes graves.
							</p>
							
							<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
							<br class="clear">
						</div>
					</div>
					<br class="clear">
					<br class="clear">
					<div  class="mpi-produtos-2">
						<br class="clear">
						<div class="col-1"><br></div>
						<div class="col-6">
							<h2>Quadro elétrico</h2>
							<p>O painel elétrico é conhecido como quadro elétrico, e está presente no dia-a-dia de muitas pessoas. Por via deles é possível obter energia fornecida pela rede elétrica de baixa tensão.
								Os <strong>quadros elétricos</strong> possuem um papel muito importante nas grandes instalações elétricas, pois por via deles se faz a passagem dos cabos vindos dos relógios de medição. Na sua instalação encontramos os disjuntores principais, ou seja, dispositivos seccionadores que alimentam circuitos tais como, lâmpadas, tomadas, iluminação, eletrodomésticos ou os que energizam outros quadros da rede elétrica do local.
							O equipamento é submetido a ensaios de características TTA ou PTTA em vigor à norma NBR IEC 60439-1, atestando que os quadros que são expostos a altas temperaturas, corrente suportável de curto-circuito, ensaios dielétricos e funcionamento mecânico.</p>
							<h2>Montagem de Quadros Elétricos</h2>
							<p>Para que seja realizada uma boa montagem de quadros elétricos, é necessário que sejam utilizados materiais elétricos de qualidade, e essa operação deve ser feita por profissionais experientes que assegurem a qualidade do procedimento e a segurança do local.
							Uma montagem de quadro elétrico deve atender às normas técnicas de segurança vigentes, bem como a NBR 5410, pois, dessa forma, a segurança de quem utilizará o quadro elétrico está garantida. Depois de escolher uma empresa confiável, o cliente deve escolher o tipo de quadro elétrico mais se adequá ao seu uso, como QGBT (Quadro Geral de Baixa Tensão), QTA (Quadro de Transferência Automática) e etc.</p>
						</div>
						<div class="col-4">
							<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
						</div>
						<div class="col-1"><br></div>
					</div>
					<hr>
					<div  data-anime="in">
						<?include('inc/tabela.php');?>
					</div>
					<span class=" btn-produto" >PDF </span>
					<hr>
					<div class="wrapper-fixa">
						<p>
							<?=$desc?>
						</p>
						<? include('inc/galeria-fixa-mpi.php');?>
						<br class="clear">
					</div>
					<br class="clear">
					<? include('inc/form-mpi.php');?>
					
				</section>
			</div>
		</main>
	</div>
	<!-- .wrapper -->
	<? include('inc/footer.php');?>
</body>
</html>