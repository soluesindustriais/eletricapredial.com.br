<? $h1 = "Painel CLP"; $title  = "Painel CLP"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Nos dias de hoje, as indústrias têm investido em automação para dar mais agilidade no processo de produção. Esta é uma tendência mundial e quem está de fora perde em competitividade. A mesma oferece muitas vantagens para empresa que adota, como agilidade; redução de custos; baixo custos com mão de obra; aumento na produção; entre outras vantagens.
							O <strong>Controlador Lógico Programável</strong>, mais conhecido como CLP, é um dos controladores mais utilizados nas indústrias, que tem como responsabilidade monitorar os processos. Com a finalidade de otimizar procedimentos, manter o nível de qualidade e de velocidade de produção.
							Não são apenas as empresas de grande porte que precisam de uma automação. As de médio e pequeno também necessitam desta ferramenta. Sendo assim, muitas vezes estas aplicações demandam um produto capaz de atender as especificações de interface de processo e conectividade.
							Essas <strong>CLP's</strong> são totalmente compactos, fabricadas em alumínio, feitas para serem instaladas em trilho DIN TS35 e possuem proteção IP30.
						</p>
						<
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6">
						<h2>Controle de processos</h2>
						<p>O tipo mais conhecido nas indústrias, é a <strong>programação de CLP</strong> com a IHM que disponibiliza ao usuário detalhes que ajudam na análise de processos diversos, permitindo que um operador consiga visualizar e modificar os parâmetros da logística e/ou processos.
							O IHM é um equipamento com uma espécie de tela, que facilita a comunicação entre o homem e a máquina, por isso da sigla IHM (Interface Homem-Máquina).
							Já o tipo industrial é um computador especial que utiliza um software para controlar todo processo das indústrias. Cada organização tem seu próprio software que é usado unicamente nesta automação. Ele pode ser customizado de acordo com a infra-estrutura do ambiente que precisa da automação. Além conter o protocolo proprietário SCP-HI implementado, o produto também possui suporte para os protocolos MODBUS-RTU, MODBUS-TCP e ASCII.
							<h2>Instalação elétrica</h2>
							<p>Existem diversos fabricantes <strong>CLP automação</strong> no Brasil. O cliente deve se atentar, principalmente, na qualidade da matéria-prima e nos serviços que a prestadora de serviços realiza a outros clientes, sempre prezando por excelência, bons profissionais e tecnologia de ponta.</p>

						</div>
						<div class="col-4">
							<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
						</div>
						<div class="col-1"><br></div>
					</div>
					<hr>
					<div  data-anime="in">
						<?include('inc/tabela.php');?>
					</div>
					<span class=" btn-produto" >PDF </span>
					<hr>
					<div class="wrapper-fixa">
						<p>
							<?=$desc?>
						</p>
						<? include('inc/galeria-fixa-mpi.php');?>
						<br class="clear">
					</div>
					<br class="clear">
					<? include('inc/form-mpi.php');?>
					
				</section>
			</div>
		</main>
	</div>
	<!-- .wrapper -->
	<? include('inc/footer.php');?>
</body>
</html>