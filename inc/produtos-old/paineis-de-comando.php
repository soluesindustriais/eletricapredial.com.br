<? $h1 = "Painel de comando"; $title  = ">Painel de comando elétrico"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O <strong>painel de comando elétrico</strong> é um método utilizado em grande parte das empresas de ramo industrial como meio de controle e organização de toda a fiação e cabos de energia que rodeiam aquele ambiente. É através desse tipo de equipamento que os colaboradores de uma determinada empresa podem solucionar qualquer problema relacionado a instalação elétrica do local.
						Mesmo tendo um papel importante para a indústria, não é apenas em ambientes empresariais que esse produto se encontra, em pequenos comércios e residências de todos os tipos, como casas e apartamentos, o <strong>quadro de distribuição elétrica</strong> também está presente, geralmente escondido em algum ponto estratégico ou embutido na parede. Em ambientes como esse, ele possui a mesma função que a realizada na indústria, porém em uma escala menor de funcionamento.</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6">
						<h2>Painel elétrico</h2>
						<p>No dia a dia do lar, esse é um dispositivo que passa despercebido por nossos olhos, mas é através do <strong>painel elétrico industrial</strong> que empresas de todos os segmentos conseguem realizar a distribuição de energia elétrica para vários circuitos individuais ou pontos de consumo. Normalmente, esse tipo de maquinário possui uma única fonte de alimentação de entrada, incluindo um disjuntor principal e um dispositivo de proteção de fuga que pode ser de corrente residual ou terra.
						Os painéis elétricos ainda apresentam mais uma facilidade, os disjuntores e as placas de distribuição podem ser utilizados para fornecer tensões monofásicas, bifásicas ou trifásicas, dessa forma você consegue adaptar as instalações elétricas para que chegue até os equipamentos a energia exata para seu  pleno funcionamento, evitando gastos extras com energia elétrica.</p>
						<h2>Painel de distribuição</h2>
						<p>O <strong>quadro de distribuição</strong> é um equipamento muito comum na maioria das instalações industriais, sua principal função é permitir que os circuitos individuais possam extrair energia de um disjuntor pré determinado, para que em caso de panes nenhum outro equipamento seja danificado.
						Para que tudo isso aconteça da melhor forma a <strong>caixa de distribuição</strong> deve estar conectada a um painel elétrico de comando, que por sua vez é alimento com um cabo de energia na entrada. A partir desse ponto a potência é dividida entre vários pequenos disjuntores, que direcionam a potência de alimentação para diferentes pontos de consumo ou circuitos.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<span class=" btn-produto" >PDF </span>
				<hr>
				<div class="wrapper-fixa">
					<p>
						<?=$desc?>
					</p>
					<? include('inc/galeria-fixa-mpi.php');?>
					<br class="clear">
				</div>
				<br class="clear">
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>