<? $h1 = "Quadro elétrico industrial"; $title  = "Quadro elétrico industrial"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O <strong>quadro de distribuição de energia</strong> é uma caixa que distribui a eletricidade dentro de uma construção, composta por chaves, fusíveis e disjuntores. Ele nada mais é do que um painel onde adentram os cabos vindos do relógio de medição e onde se faz a distribuição dos circuitos elétricos.
							É nele também onde se encontram os disjuntores que são os mecanismos de proteção contra curtos-circuitos e sobrecarga, juntamente com a barra de aterramento. Ao confeccionar o quadro nunca esqueça de deixar espaço livre para futuras ampliações. O aterramento é muito importante, porque além de proteger os equipamentos também protege os usuários dos mesmos.
						</p>
						
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6">
						<h2>Quadro distribuição</h2>
						<p>Nas instalações elétricas, o <strong>quadro de distribuição</strong> é o componente responsável por abrigar um ou mais dispositivos de proteção (e/ou de manobra) e a conexão de condutores elétricos interligados a estes elementos, com a finalidade de distribuir a energia aos diversos circuitos da edificação. É de grande importância que toda instalação elétrica seja dividida em vários circuitos, devendo cada um deles ser concebido de forma a poder ser seccionado sem risco de realimentação inadvertida.
							Os quadros podem ser separados em alguns tipos: terminais, circuitos alimentadores e os de distribuição. Os <strong>quadros de força</strong> são aqueles que alimentam exclusivamente circuitos terminais. Já os de distribuição, são os quadros de onde partem um ou mais circuitos alimentadores ou terminais.
						Os circuitos terminais devem ser projetados sem serem sobrecarregados, pois a elevada potência resulta na necessidade de condutores de grande seção nominal, o que dificulta a execução da instalação dos fios nos eletrodutos e as ligações aos terminais dos aparelhos de utilização, como os interruptores, tomadas e luminárias.</p>
						<h2>Quadro de distribuição elétrica com barramento</h2>
						<p>O <strong>barramento para quadro de distribuição</strong> é um produto de cobre eletrolítico. Várias dificuldades são encontradas somente no barramento, pois a falta de manutenção ou má montagem dos quadros de distribuição trazem transtornos a toda instalação elétrica.
							O <strong>quadro elétrico com barramento</strong> é montado de acordo com todas as normas necessárias. Esse tipo de quadro serve para a segurança contra as fugas de descargas elétricas, pois a levam ao solo ignorando qualquer risco de acidente com as pessoas ao redor do quadro.
						O barramento de fase é encarregado pela distribuição, aos circuitos elétricos, da corrente, e por último, o barramento de neutro que realiza a ligação ao ponto zero de cada circuito, através de fios e terminais.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<span class=" btn-produto" >PDF </span>
				<hr>
				<div class="wrapper-fixa">
					<p>
						<?=$desc?>
					</p>
					<? include('inc/galeria-fixa-mpi.php');?>
					<br class="clear">
				</div>
				<br class="clear">
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>