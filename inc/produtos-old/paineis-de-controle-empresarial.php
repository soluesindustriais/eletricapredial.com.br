<? $h1 = "Painel de controle empresarial"; $title  = "Painel de controle empresarial"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Um apartamento, mesmo sendo, em alguns casos, um imóvel pequeno, é um ambiente que exige um certo planejamento quando a implementação de energia elétrica.
							Quando pensamos nas instalações de um empresa, onde normalmente diversos computadores passarão grande parte do dia conectados a tomadas. Ou em locais industriais, onde o funcionamento de máquinas de grande porte, acontece sem qualquer pausa, um <strong>projeto elétrico industrial</strong> é essencial.
							A partir dele serão criadas tomadas e interruptores em locais estratégicos, a fiação será direcionada para que as tensões certas cheguem até as tomadas, e por fim, será desenvolvido um painel elétrico, responsável por armazenar e organizar todos com componentes elétricos.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6">
						<h2>Projeto Elétrico</h2>
						<p>Apesar de existirem alguns modelos padrões de <strong>painel de distribuição</strong> é muito difícil encontrar um que seja exatamente igual ao outro. Como cada máquina funciona de um modo diferente e cada operador sente a necessidade de controlar o equipamento de uma forma, os painéis são ajustados para suprir essas necessidades.
						Um projeto industrial elétrico bem realizado, além de coordenar a construção de um equipamento como essa, ainda auxiliará na elaboração e instalação de outros componentes que podem ou não fazer parte da composição do <strong>painel elétrico</strong>.</p>
						<h2>Controlador Lógico Programável</h2>
						<p>Funcionando de forma muito semelhante a um computador, o <strong>CLP</strong> é um dos componentes básicos para a desenvolvimento de um painel elétrico. Composto por uma CPU, memória RAM e ROM e portas de comunicação, o que diferencia esse equipamento de um computador tradicional é que ele foi projetado para trabalhar em condições industriais extremas e ambientes agressivos.
						Isso quer dizer que o CLP consegue suportar poeira, temperaturas extremas e vibrações. Além disso, existem outras vantagens provenientes deste equipamento, como a característica de ser flexível, e a possibilidade de inserção de módulos de entradas e saídas, ou até mesmo comunicação com diferentes aplicações, permitindo a interface com vários dispositivos de chão de fábrica.
						O CLP funciona como o cérebro do <strong>painel de montagem</strong>, é através dele que todas as ações são coordenadas, entendida pelos outros componentes e executadas.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<span class=" btn-produto" >PDF </span>
				<hr>
				<div class="wrapper-fixa">
					<p>
						<?=$desc?>
					</p>
					<? include('inc/galeria-fixa-mpi.php');?>
					<br class="clear">
				</div>
				<br class="clear">
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>