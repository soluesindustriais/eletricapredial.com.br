<? $h1 = "Quadro de comandos elétricos"; $title  = "Automação de quadros elétricos"; $desc = "Ofertas incríveis de $h1, você acha nos resultados das buscas do Soluções Industriais, cote produtos pela internet com mais de 200 fornecedores ao mesmo tempo"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php'); include('inc/fancy.php'); ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main role="main">
		<div class="content">
			<section>
				<br class="clear">
				<?=$caminhoquadro_eletrico?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/comprar-qta.jpg" alt="" data-anime="in">
					</div>
					<div class="col-7">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Quadros de comandos elétricos são fundamentais para o bom funcionamento de equipamentos comerciais ou industriais. São eles os responsáveis, através da seleção entre funcionamento automático e funcionamento manual, pelo comando do funcionamento do equipamento.
						</p>
						<p>O quadro de distribuição elétrica é composto por diversos aparelhos de proteção e manobra, unido em uma ou mais colunas adjacentes, devendo ser assemblado de forma apropriada. Esse tipo de quadro consiste num contentor e de uma aparelhagem elétrica representada pelos aparelhos, as conexões internas e os bornes para entrada e saída da instalação.
							Os quadros também protegem seus equipamentos contra eventuais faltas de fase, e agem na prevenção de erros de manobra e na proteção das causas de queima de motores, além de diversas outras funções.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6">
						<h2>Montagem de quadro elétrico </h2>
						<p>Quadros elétricos são compostos geralmente por caixas metálicas, disjuntores motores, proteção contra sobrecarga, proteção contra falta de fase, entre outros elementos. É importante realizar a montagem de painel elétrico  de forma correta, pois esse procedimento será responsável por distribuir a energia para caixa de transferência de luz. De forma prática, ele recebe a carga de energia da rua, filtra a distribuição e leva a partir disso para o quadro de distribuição de luz, que fica em residências, ou locais de pequeno porte.
							Além disso a montagem feita por profissionais garante excelentes condições ambientais de serviços e compreende todos os requisitos mecânicos e elétricos. Todas as partes do quadro elétrico passam por uma revisão após a montagem e, quando necessário, é realizada a reforma nos mesmos, deixando o equipamento totalmente renovado.
						</p>
						<h2>Automação industrial </h2>
						<p>A automação de quadros elétricos é constituída por um sistema de mecanismos de um quadro que são adaptados para que se tornarem capazes de realizar o controle de seu próprio funcionamento. Assim, o equipamento passa a realizar de uma maneira automática todas as tarefas que exigem complexidade, ou tem necessidade de repetições contínuas, entre outros casos que não compensa fazer atribuição de execução a mão-de-obra humana a automação é preferida.
						Por meio de uma automação industrial no quadro, o cliente permite que sua empresa explore o máximo de recursos tecnológicos em seu quadro elétrico, permitindo que seus funcionários dediquem-se em processos onde realmente se faz necessário.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/comprar-quadro-eletrico.jpg" alt="" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<span class=" btn-produto" >PDF </span>
				<hr>
				<div class="wrapper-fixa">
					<p>
						<?=$desc?>
					</p>
					<? include('inc/galeria-fixa-mpi.php');?>
					<br class="clear">
				</div>
				<br class="clear">
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>