
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script  src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Comprar painel eletrônico de senha','Kit painel de senhas','Painel de chamada de senha','Painel de senha','Painel de senha aleatória','Painel de senha alternada','Painel de senha atendimento','Painel de senha com impressora','Painel de senha com teclado','Painel de senha com tv','Painel de senha digital','Painel de senha digital preço','Painel de senha e guichê','Painel de senha eletrônica','Painel de senha eletrônica preço','Painel de senha na tv','Painel de senha para atendimento','Painel de senha para lanchonete','Painel de senha para restaurante','Painel de senha preço','Painel de senha sequencial','Painel de senha sequencial e aleatória','Painel de senhas eletrônico','Painel e impressora de senha','Painel eletrônico de chamada de senhas','Painel eletrônico de senha','Painel eletrônico de senha digital','Painel eletrônico de senha manual','Painel eletrônico de senha sequencial','Painel eletrônico de senhas para atendimento','Painel eletrônico indicador de senha','Painel eletrônico para senhas','Painel eletrônico senha','Painel senha','Painel senha eletrônica','Quanto custa um painel eletrônico de senha'); shuffle($lista); for($i=1;$i<13;$i++){ ?>
<?php
$folder = "painel-de-senha";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/painel-de-senha/painel-de-senha-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-06"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/painel-de-senha/painel-de-senha-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/painel-de-senha/thumbs/painel-de-senha-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>