
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script  src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Montador de painel','Painéis eletrônicos de led','Painéis eletrônicos de propaganda','Painel contador eletrônico de peças','Painel contador eletrônico digital','Painel controlador eletrônico de velocidade','Painel de satisfação eletrônico','Painel de votação eletrônico','Painel eletrônico','Painel eletrônico a venda','Painel eletrônico alfanumérico','Painel eletrônico chamada fila única','Painel eletrônico com arduíno','Painel eletrônico contador de vagas de estacionamento','Painel eletrônico cronometro','Painel eletrônico de chamada de fila única','Painel eletrônico de cipa','Painel eletrônico de fila hospitalar','Painel eletrônico de led preço','Painel eletrônico de leds','Painel eletrônico de leds informativo','Painel eletrônico de mensagens','Painel eletrônico de metas','Painel eletrônico de produção','Painel eletrônico de rodovia','Painel eletrônico de votação','Painel eletrônico digital','Painel eletrônico digital led','Painel eletrônico digital para propaganda','Painel eletrônico fila','Painel eletrônico fila única','Painel eletrônico indicador de satisfação','Painel eletrônico inteligente','Painel eletrônico led','Painel eletrônico led para publicidade','Painel eletrônico led preço','Painel eletrônico letreiro luminoso led','Painel eletrônico móvel de carreta','Painel eletrônico ncm','Painel eletrônico numérico para orientação de filas','Painel eletrônico para posto de combustível','Painel eletrônico para propaganda','Painel eletrônico para propaganda preço','Painel eletrônico para queimador de palha','Painel eletrônico preço','Painel multilinhas eletrônico','Placar eletrônico esportivo preço','Preço de painel eletrônico de mensagem'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "painel-eletronico";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/painel-eletronico/painel-eletronico-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-06"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/painel-eletronico/painel-eletronico-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/painel-eletronico/thumbs/painel-eletronico-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>