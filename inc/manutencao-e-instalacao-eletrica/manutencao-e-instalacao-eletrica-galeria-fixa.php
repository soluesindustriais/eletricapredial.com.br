
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('empresa de instalação elétrica industrial','empresa de manutenção elétrica','instalação e manutenção elétrica','instalação elétrica em galpões industrial','manutenção de instalações elétricas','manutenção de sistemas elétricos','manutenção elétrica de processos industrial','manutenção elétrica predial e industrial','manutenção preventiva e corretiva instalações elétricas','serviços de instalação e manutenção elétrica','serviços de manutenção elétrica industrial','empresas de manutenção eletrica industrial','manutenção elétrica predial','empresas de manutenção de redes eletricas','instalação e manutenção elétrica simples nacional'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "manutencao-e-instalacao-eletrica";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/manutencao-e-instalacao-eletrica/manutencao-e-instalacao-eletrica-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-06"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/manutencao-e-instalacao-eletrica/manutencao-e-instalacao-eletrica-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/manutencao-e-instalacao-eletrica/thumbs/manutencao-e-instalacao-eletrica-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>