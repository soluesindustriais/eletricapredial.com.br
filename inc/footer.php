<footer>
	<div class="wrapper-footer">
		<div class="contact-footer">
			<address>
				<span><?=$nomeSite." - ".$slogan?></span>
			</address>
			<?php 
			if ($urlPagina != '') {
				include'inc/apisolucs.php';
			}
			 ?>
			<br>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<li><a href="<?=$url?>" title="Página inicial">Inicio</a></li>
					<li><a href="<?=$url?>sobre-nos" title="Sobre nós">Sobre nós</a></li>
					<li><a href="<?=$url?>produtos-categoria" title="Produtos <?=$nomeSite?>">Produtos</a></li>
					<li><a rel="nofollow" href="<?=$url?>mapa-site" title="Mapa do Site">Mapa do Site</a></li>
          <li><a  class="buy-button" target='_blank' href="https://faca-parte.solucoesindustriais.com.br" title="faça parte">Gostaria de anunciar?</a></li>
				</ul>
			</nav>
		</div>
	</div>
</footer>
<div class="copyright-footer">
	<div class="wrapper-copy">
		Copyright ©
		<?=$nomeSite?>. (Lei 9610 de 19/02/1998)
			<div class="center-footer img-logo">
				<img src="imagens/logo-top-1-tiny-white.png" alt="Elétrica Predial" title="Elétrica Predial" >
				<p>é um parceiro</p>
				<img src="imagens/logo-solucs.png" alt="Soluções Industriais" title="Soluções Industriais">
			</div>
			<div class="selos">
				<a rel="nofollow" href="https://validator.w3.org/check?uri=<?=$url.$urlPagina?>" target="_blank" title="HTML5 W3C"><i class="fab fa-html5"></i> <strong>HTML 5 W3C</strong></a>
				<a rel="nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?=$url.$urlPagina?>" target="_blank" title="CSS W3C"><i class="fab fa-css3"></i> <strong>CSS W3C</strong></a>

			</div>
	</div>
</div>
<script defer src="<?=$url?>js/geral.js"></script>
<script src="<?=$url?>js/jquery.scrollUp.min.js"></script>
<script src="<?=$url?>js/scroll.js"></script>
<script src="<?=$url?>js/app.js"></script>


<script>
function leiaMais() {
  var dots = document.getElementById("final");
  var moreText = document.getElementById("mais");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Leia Mais"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Ocultar"; 
    moreText.style.display = "inline";
  }

}
</script>

<script>
  function toggleDetails() {
    var detailsElement = document.querySelector(".webktbox");

    // Verificar se os detalhes estão abertos ou fechados
    if (detailsElement.hasAttribute("open")) {
      // Se estiver aberto, rolar suavemente para cima
      window.scrollTo({ top: 200, behavior: "smooth" });
    } else {
      // Se estiver fechado, rolar suavemente para baixo (apenas 100px)
      window.scrollTo({ top: 1300, behavior: "smooth" });
    }
  }
</script>


<script>
	(function(){ var widget_id = 'GOChF5AUsR';var d=document;var w=window;function l(){
  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
  s.src = '//code.jivosite.com/script/widget/'+widget_id
    ; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
  if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
  else{w.addEventListener('load',l,false);}}})();
</script>

<script>
var guardar = document.querySelectorAll('.botao-cotar');
for(var i = 0; i < guardar.length; i++){
guardar[i].removeAttribute('href');
  var adicionando = guardar[i].parentNode;
  adicionando.classList.add('nova-api');
};
</script>


<script async src="https://www.googletagmanager.com/gtag/js?id=G-HZDMTML4FK"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-HZDMTML4FK');
</script>


<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-D23WW3S4NC');
</script>
        <div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>