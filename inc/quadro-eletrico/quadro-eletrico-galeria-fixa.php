
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script  src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Automação de quadros elétricos','Barramento para quadro elétrico','Disjuntores e quadros de distribuição','Distribuidor de quadros elétricos','Empresas de quadros elétricos','Fabricante de quadros elétricos','Fornecedores de quadros elétricos','Instalação de quadro de distribuição','Instalação de quadro de distribuição industrial','Instalação de quadro de distribuição trifásico','Instalação de quadros elétricos','Instalação de quadros elétricos preço','Instalação e montagem de quadros elétricos','Manutenção de quadros de peneiras','Montagem de quadro de baixa tensão','Montagem de quadro de comando','Montagem de quadro de comando elétrico','Montagem de quadro de distribuição','Montagem de quadro de distribuição elétrica','Montagem de quadro de distribuição elétrica residencial','Montagem de quadro elétrico sp','Montagem de quadros elétricos','Painéis e quadros elétricos','Preço do esquadro para serralheiro','Quadro andon','Quadro andon de produção','Quadro de comando elétrico','Quadro de comando elétrico preço','Quadro de comando elétrico residencial','Quadro de controle','Quadro de distribuição de força e luz','Quadro de distribuição de luz','Quadro de distribuição siemens','Quadro de energia','Quadro elétrico','Quadro elétrico fotovoltaico','Quadro geral de distribuição de força','Reforma de quadro elétrico','Valor de quadro elétrico','Venda de quadros e painéis','Venda de quadros e painéis elétricos'); shuffle($lista); for($i=1;$i<13;$i++){ ?>
<?php
$folder = "quadro-eletrico";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/quadro-eletrico/quadro-eletrico-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-06"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/quadro-eletrico/quadro-eletrico-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/quadro-eletrico/thumbs/quadro-eletrico-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>