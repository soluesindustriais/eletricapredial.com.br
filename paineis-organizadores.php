<? $h1 = "Painéis organizador"; $title  = "Painéis organizador"; $desc = "Ordene os materiais elétricos da sua empresa com o painel organizador, ele trará mais proteção para seus colaboradores. Cote com a Elétrica Predial agora!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Ultimamente é de grande importância aumentar a eficiência no atendimento ao público, por conta disso foi criado o <strong>painel eletrônico de senha</strong>. Durante um certo tempo vemos painéis programados para auxiliar filas em caixas de banco e em supermercados. Agora, a mesma tecnologia está sendo usada para melhorar o atendimento na área de restaurantes e lanchonetes.
							Existem diversos painéis organizadores, um deles é o <strong>painel eletrônico de senha</strong>, com esse sistema os clientes aguardam serem chamados pelo próximo caixa ou guichê livre. É o sistema dos caixas rápidos em supermercado, por exemplo. O "painel de senha" pode ter dois ou mais dígitos que sinalizam o próximo caixa disponível.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Painel eletrônico</h2>
						<p>Para que esse tipo de painel funcione corretamente, é imprescindível que o atendente tenha pleno controle de chamadas de senhas. Para isso, basta que o mesmo aperte o botão no controle para chamar o próximo da fila no painel. Simples e ágil.
						O <strong>painel de senha eletrônica</strong> possui chamadas de senha entre 001 e 999, além disso, possibilita maior organização dentro de estabelecimentos de médio e grande porte.</p>
						<h2>Painel eletronico de led</h2>
						<p>Existem diferentes tipos de tecnologias que podem ser utilizadas nos Leds, uma delas é a tecnologia EDGE, que possibilita ter esses leds somente nas bordas, viabilizando que os televisores e os painéis mais finos operem com capacidade, de forma mais leve e com um menor gasto de energia.
							Com esse tipo de painel, a tecnologia FULL Led, que significa leds em todo o <strong>painel de led</strong> e possibilita uma melhor capacidade da imagem, especialmente a níveis de preto quando aliado a outra tecnologia chamada local dimming. O local dimming em uma full led gera centenas de zonas independentes que possibilitam desligar os leds que não são usados, criando pretos perfeitos e também minimizando o backlight bleeding.
							Por serem de extrema importância no ramo comercial, esses <strong>painéis eletrônicos</strong> possuem diversos benefícios e características, que fazem com que o mesmo tenha funcionamento correto.
						Algumas características são: grande visibilidade: 30m (dígitos de 5cm) ou 60m (dígitos de 10cm), memória: Possibilidade de gravar a última senha na falta de energia, sua alimentação: Bivolt – 110 / 220 V 60 Hz, possui gabinete: Em ABS e acrílico vermelho antirreflexo, pode ter sinal sonoro: Bitonal, com volume regulável e acionamento: Radiofrequência – via controle remoto sem fio.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg" alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-mpi.php');?>
<button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar">Orçamento Grátis</button>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>