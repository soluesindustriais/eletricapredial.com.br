<? $h1 = "Painéis eletrônicos preço"; $title  = "Painel Eletrônico de LED | Elétrica Predial"; $desc = "Está com dificuldade de encontrar um painel eletrônico? Na Elétrica Predial você cota com várias empresas e compra com a que preferir. Confira já!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Um <strong>painel eletrônico de LED</strong> é fabricado utilizando-se milhares, ou centenas de milhares, de pequenos LEDs para formar as imagens e vídeos ali exibidos. Por ter um brilho mais intenso e uma maior vida útil, é muito utilizado em locais como hospitais, universidades, salas de espera, entre outros.
							O <strong>painel eletrônico</strong> é um dispositivo usado para exibir informações, ele pode ser integrado a sistemas, o que abre um leque enorme de opções para apresentar informações tanto de filas como mensagens rápidas.
							Os <strong>painéis eletrônicos</strong> são capazes de exibir informações em uma distância de até 50 metros, grande parte dos modelos possuem relógio e data ajustados por acionador de radiofrequência. Utiliza fonte de alimentação externa de 110/220 V e suportes tipo olhal para sua fixação.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Painel de senha eletrônica</h2>
						<p>O painel de senha eletrônica é utilizado geralmente em ambientes que precisam de um controle de atendimento, é muito comum encontrar esse tipo de painel em hospitais, universidades, salas de espera, restaurantes, entre outros.
							O objetivo desse tipo de painel é organizar e priorizar atendimentos, conectado ao painel está um sistema que emite senhas de papel e com esse “ticket” você acompanha seu atendimento. O painel eletrônico é totalmente ajustável e está disponível em diversas cores, se adequando assim, a necessidade de cada pessoa.
						Os mais comuns dos modelos de painéis eletrônicos disponíveis no mercado hoje são os <strong>painéis em LED</strong>, esse tipo de tecnologia permite a visibilidade em grandes distâncias e acaba sendo formado por centenas e às vezes milhares de lâmpadas LED's para deixar a informação mais clara possível.</p>
						<h2>Painel led</h2>
						<p>Um painel LED é fabricado utilizando-se milhares, ou centenas de milhares, de pequenas lâmpadas em LEDs para formar as imagens e vídeos ali exibidos. Por ter um brilho mais intenso e uma maior vida útil, é muito utilizado pelo segmento out-of-home.
						Os <strong>letreiro de LED</strong> são capazes de transmitir mensagens alfanuméricas. Podem ser utilizados em diversos segmentos a fim de comunicar, alertar ou para fazer uma propaganda ou publicidade. Esse tipo de painel garante praticidade e rapidez na comunicação com seus colaboradores e clientes. Ideais para ambientes internos ou externos, programado via software.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg" alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-eletrico.php');?>
<button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar">Orçamento Grátis</button>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>