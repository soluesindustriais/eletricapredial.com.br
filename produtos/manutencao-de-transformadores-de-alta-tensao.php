<?php

$minisite = "bralux-transformadores";
$linkminisite = "inc/$minisite/";
$subdominio = "bralux-transformadores";

$clienteAtivo = "ativo";
$linkminisitenb = substr($linkminisite, 0, -1);
$clienteAtivo = "ativo";

// Obtém o nome do arquivo atual sem a extensão
$nomeArquivo = pathinfo(basename(__FILE__), PATHINFO_FILENAME);

// Remove os hífens e transforma a primeira letra de cada palavra em maiúscula
$nomeVariavel = ucwords(str_replace('-', ' ', $nomeArquivo));

$h1 = $nomeVariavel;
$title = $nomeVariavel;
$desc           = "Garanta a segurança e eficiência da distribuição de energia com a manutenção de transformadores de alta tensão. Prevenção de falhas e prolongamento da vida útil.";
include ("$linkminisite"."inc/head.php");
include ("$linkminisite"."inc/fancy.php");
?>
<style>
    <?
    include "$linkminisite" . "css/header-script.css";
    include "$linkminisite" . "css/style.css";
    include "$linkminisite" . "css/mpi.css";
    include "$linkminisite" . "css/normalize.css";
    include "$linkminisite" . "css/aside.css";
    ?>
</style>

</head>

<body>
    <? include "$linkminisite" . "inc/header-dinamic.php"; ?>
    <main class="main-tag-content">
        <div class="content" itemscope itemtype="https://schema.org/Article">
            <section>
                <?= $caminhoservicos ?>
                <div class="wrapper main-mpi-container">
                    <article class="description">
                        <div class="article-content">
                            <div class="ReadMore" style="overflow: hidden; height: auto; transition: height 100ms ease-in-out 0s;">
                                <h2 class="h2-description">Descrição</h2>
                                <p class="p-description">
                                    <?= $conteudoPagina[0] ?>
                                </p>
                                <span id="readmore-open">Continuar Lendo...</span>
                                <span id="readmore-close">Fechar <i class="fa-solid fa-turn-up" style="color: var(--azul-solucs);"></i></span>
                            </div>
                        </div>
                        <?
                        include "$linkminisite" . "inc/gallery.php";
                        ?>
                        <?
                        include "$linkminisite" . "inc/card-informativo.php";
                        // include("inc/especificacoes-mpi.php");
                        ?>
                    </article>
                    <?
                    include "$linkminisite" . "inc/coluna-lateral.php";
                    include "$linkminisite" . "inc/regioes.php";
                    include "$linkminisite" . "inc/aside-produtos.php";

                    include "$linkminisite" . "inc/copyright.php";
                    ?>
                </div><!-- .wrapper -->
                <div class="clear"></div>
            </section>
        </div>
    </main>
    <? include "$linkminisite" . "inc/footer.php"; ?>
    <script src="<?=$linkminisite?>js/organictabs.jquery.js" async></script>

</body>


</html>