<? $h1 = "Informações";
$title  = "Informações";
$desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include("inc/head.php");
include("inc/informacoes/informacoes-vetPalavras.php"); ?>

<style>
  body {
    scroll-behavior: smooth;
  }

  <?
  include('css/header-script.css');
  include("$linkminisite" . "css/style.css");
  ?>
</style>
</head>

<body> <? include("inc/header-dinamic.php"); ?><main role="main">
    <section> <?= $caminho ?> <div class="wrapper-produtos"> <br class="clear">
        <h1 style="text-align: center;  "><?= $h1 ?></h1>
        <article class="full">
          <div class="article-content">

            <p>A Bralux Transformadores é especializada na venda, reforma e locação de transformadores e equipamentos elétricos para subestações. Também oferecemos manutenção preventiva e corretiva em subestações, assim como prestação de outros serviços em diversos equipamentos elétricos.</p>
            <p>A Bralux Transformadores possui mão de obra experiente e alta tecnologia para garantir o bom funcionamento de transformadores elétricos, novos e recondicionados, com garantia de qualidade garantida.</p>
            <p>Essa amplitude de oferta permite identificar sinergias e oportunidades para atender, de maneira integrada, às necessidades de nossos clientes, sejam concessionárias de energia elétrica, cooperativas de eletrificação, indústrias ou grandes usuários de energia.</p>

          </div>
          <ul class="thumbnails-main"> <?php include_once("inc/informacoes/informacoes-categoria.php"); ?> </ul>
        </article>
    </section>
  </main>
  </div>
  <!-- .wrapper --> <? include("inc/footer.php"); ?> </body>

</html>