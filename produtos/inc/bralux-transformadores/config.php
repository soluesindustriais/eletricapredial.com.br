<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Bralux Transformadores';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Comprometida com a segurança';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '', $tituloCliente);


//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";

// Conteudo das páginas
$conteudoPagina = [
"<strong>Manutenção de Transformadores de Alta Tensão: Garantia de Segurança e Eficiência</strong><br>
A <strong>manutenção de transformadores de alta tensão</strong> é essencial para garantir a segurança e eficiência na distribuição de energia elétrica. Esses equipamentos são cruciais para o funcionamento de redes elétricas, e a manutenção adequada previne falhas, prolonga a vida útil dos transformadores e assegura um fornecimento estável de energia.<br>
<strong>Importância da Manutenção</strong><br>
A manutenção regular ajuda a identificar e corrigir problemas antes que se tornem críticos, evitando paradas inesperadas e danos mais graves. Inspeções periódicas, testes de óleo isolante, limpeza e reaperto de conexões são algumas das atividades essenciais. Essas práticas garantem que o transformador opere dentro dos parâmetros seguros e eficientes.<br>
<strong>Procedimentos de Manutenção</strong><br>
Os procedimentos de manutenção incluem a verificação do nível e da qualidade do óleo isolante, que é fundamental para o isolamento e resfriamento do transformador. Além disso, a inspeção visual para detectar sinais de desgaste, corrosão ou vazamentos é crucial. Testes elétricos, como a medição da resistência de isolamento e a análise de gases dissolvidos no óleo, também são realizados para avaliar a condição interna do equipamento.<br>
<strong>Dicas para uma Manutenção Eficiente</strong><br>
Para garantir uma manutenção eficiente, é importante seguir um cronograma rigoroso e utilizar equipamentos de teste de alta precisão. Contratar profissionais qualificados e experientes é crucial para realizar uma manutenção segura e eficaz. Além disso, a documentação detalhada de cada manutenção ajuda a monitorar a performance do transformador ao longo do tempo.<br>
Invista na manutenção de transformadores de alta tensão para assegurar a continuidade e a qualidade do fornecimento de energia elétrica.",

"<strong>Capacitor de Potência: Eficiência e Qualidade na Distribuição de Energia</strong><br>
Um <strong>capacitor de potência</strong> é um componente fundamental para melhorar a eficiência e a qualidade da energia elétrica em sistemas de distribuição. Utilizados para correção do fator de potência, esses capacitores ajudam a reduzir perdas de energia e a melhorar a estabilidade da rede elétrica.<br>
<strong>Vantagens do Capacitor de Potência</strong><br>
A principal vantagem do capacitor de potência é a correção do fator de potência, o que resulta em uma utilização mais eficiente da energia elétrica. Além disso, a instalação de capacitores de potência reduz a demanda de energia reativa, diminuindo as perdas de energia nas linhas de transmissão e distribuição. Isso, por sua vez, pode levar a uma redução nas tarifas de energia e a uma menor necessidade de investimento em infraestruturas de rede.<br>
<strong>Aplicações dos Capacitores de Potência</strong><br>
Capacitores de potência são amplamente utilizados em indústrias, instalações comerciais e em sistemas de energia elétrica. Eles são essenciais em locais onde grandes motores e equipamentos elétricos são utilizados, ajudando a manter a eficiência energética e a estabilidade do sistema. Além disso, capacitores de potência são usados em subestações e redes de distribuição para melhorar a qualidade da energia fornecida.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção regular dos capacitores de potência é crucial para garantir seu desempenho ideal. Isso inclui inspeções periódicas, limpeza, verificação de conexões e testes elétricos para detectar possíveis falhas. Utilizar equipamentos de proteção e seguir normas de segurança durante a manutenção é essencial para prevenir acidentes e prolongar a vida útil dos capacitores.<br>
Invista em capacitores de potência para otimizar a eficiência e a qualidade da sua distribuição de energia elétrica.",

"<strong>Equipamentos de Subestação: Essenciais para a Distribuição de Energia</strong><br>
Os <strong>equipamentos de subestação</strong> são componentes cruciais para a conversão, controle e distribuição de energia elétrica em redes de alta, média e baixa tensão. Eles garantem que a energia gerada chegue de forma segura e eficiente aos consumidores finais, desempenhando um papel vital na infraestrutura elétrica.<br>
<strong>Principais Equipamentos de Subestação</strong><br>
Transformadores: Responsáveis por alterar os níveis de tensão, os transformadores são essenciais para a transmissão e distribuição de energia elétrica. Eles reduzem a tensão para níveis adequados ao consumo ou aumentam-na para transmissão a longas distâncias.<br>
Disjuntores: Dispositivos de proteção que interrompem o fluxo de corrente em caso de sobrecarga ou curto-circuito, protegendo o sistema de danos.<br>
Seccionadores: Usados para isolar partes do circuito para manutenção, os seccionadores permitem a desconexão segura de seções da rede elétrica.<br>
Parafusos Fusíveis: Protegem os transformadores e outros equipamentos de sobrecargas e curtos-circuitos, sendo uma medida de segurança fundamental.<br>
Medidores de Tensão e Corrente: Esses dispositivos monitoram e controlam os níveis de tensão e corrente, garantindo que o sistema opere dentro dos parâmetros especificados.<br>
<strong>Importância da Manutenção</strong><br>
A manutenção dos <strong>equipamentos de subestação</strong> é crucial para garantir a confiabilidade e eficiência do fornecimento de energia. Inspeções regulares, testes de funcionamento, limpeza e reparos preventivos ajudam a evitar falhas e prolongam a vida útil dos equipamentos. Manter registros detalhados de manutenção também é importante para monitorar o desempenho e planejar intervenções futuras.<br>
<strong>Dicas para uma Manutenção Eficiente</strong><br>
Siga um cronograma de manutenção preventiva rigoroso e utilize ferramentas de teste de alta precisão. Contrate técnicos qualificados e experientes para realizar as inspeções e manutenções. Atualize constantemente os procedimentos de segurança e mantenha um estoque de peças sobressalentes para reparos rápidos.<br>
Invista em equipamentos de subestação de qualidade e em sua manutenção regular para assegurar um fornecimento de energia confiável e seguro.",

"<strong>Reforma de Transformadores: Prolongando a Vida Útil e Eficiência</strong><br>
A <strong>reforma de transformadores</strong> é um processo crucial para prolongar a vida útil e manter a eficiência desses equipamentos vitais para a distribuição de energia elétrica. Reformar um transformador pode ser mais econômico do que substituí-lo, além de ser uma prática sustentável que reduz a necessidade de novos recursos.<br>
<strong>Vantagens da Reforma de Transformadores</strong><br>
A principal vantagem da reforma é a restauração da capacidade operacional do transformador, prolongando sua vida útil e evitando custos elevados com a compra de novos equipamentos. A reforma inclui a substituição de componentes desgastados, limpeza e testes de desempenho, garantindo que o transformador opere com eficiência máxima.<br>
<strong>Etapas da Reforma de Transformadores</strong><br>
Inspeção e Diagnóstico: Uma análise detalhada do estado do transformador para identificar problemas e determinar a melhor abordagem de reparo.<br>
Desmontagem e Limpeza: Remoção de componentes para limpeza e avaliação detalhada de cada parte.<br>
Reparos e Substituições: Conserto ou substituição de peças danificadas, como bobinas, núcleos, isoladores e juntas.<br>
Reenrolamento: Se necessário, as bobinas do transformador são reenroladas para restaurar sua funcionalidade.<br>
Teste e Recondicionamento: Testes rigorosos para assegurar que o transformador atende aos padrões de desempenho e segurança.<br>
<strong>Dicas para uma Reforma Eficiente</strong><br>
Escolha uma empresa especializada e com experiência comprovada em reforma de transformadores. Utilize componentes de alta qualidade e siga rigorosamente os procedimentos de segurança. Mantenha registros detalhados de todas as etapas da reforma para futuras referências e monitoramento de desempenho.<br>
Invista na reforma de transformadores para assegurar um fornecimento de energia eficiente, prolongando a vida útil dos equipamentos e economizando recursos.",

"<strong>Regulador de Tensão Monofásico: Estabilidade e Proteção Elétrica</strong><br>
Um <strong>regulador de tensão monofásico</strong> é um dispositivo essencial para garantir a estabilidade da tensão elétrica em redes monofásicas, protegendo equipamentos contra variações e picos de energia. Ele é amplamente utilizado em residências, escritórios e pequenas indústrias para assegurar um fornecimento de energia constante e confiável.<br>
<strong>Vantagens do Regulador de Tensão Monofásico</strong><br>
A principal vantagem de um regulador de tensão monofásico é a proteção que ele oferece aos equipamentos eletrônicos, prevenindo danos causados por sobretensões, subtensões e picos de energia. Além disso, ele melhora a eficiência energética, garantindo que os aparelhos funcionem dentro dos parâmetros ideais, o que pode prolongar sua vida útil e reduzir custos com manutenção.<br>
<strong>Aplicações do Regulador de Tensão Monofásico</strong><br>
Os reguladores de tensão monofásicos são usados em uma variedade de aplicações, como em sistemas de iluminação, equipamentos de informática, aparelhos eletrodomésticos e sistemas de telecomunicações. Eles são ideais para locais onde a estabilidade da tensão é crítica para o desempenho e segurança dos dispositivos conectados.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>regulador de tensão monofásico</strong> é relativamente simples, envolvendo inspeções periódicas para verificar o estado dos componentes e assegurar que o dispositivo esteja operando corretamente. Limpezas regulares e testes de funcionamento ajudam a identificar problemas potenciais antes que eles afetem o desempenho do regulador.<br>
<strong>Dicas para Escolher um Regulador de Tensão Monofásico</strong><br>
Ao escolher um regulador de tensão monofásico, considere a capacidade de carga, a precisão de regulação e a proteção contra surtos. Certifique-se de que o modelo escolhido seja adequado para o tipo de aparelhos que serão conectados e que ofereça características como proteção contra sobrecargas e curto-circuitos.<br>
Invista em um regulador de tensão monofásico para garantir a estabilidade e proteção de seus equipamentos elétricos, assegurando um fornecimento de energia confiável e seguro.",

"<strong>Regulador de Tensão Trifásico: Estabilidade e Eficiência Energética</strong><br>
Um <strong>regulador de tensão trifásico</strong> é essencial para garantir a estabilidade da tensão elétrica em sistemas trifásicos, protegendo equipamentos industriais e comerciais contra variações e picos de energia. Ele é utilizado para assegurar um fornecimento de energia constante e eficiente, fundamental para o desempenho de máquinas e equipamentos de alta potência.<br>
<strong>Vantagens do Regulador de Tensão Trifásico</strong><br>
A principal vantagem de um regulador de tensão trifásico é a proteção que oferece aos equipamentos contra flutuações de tensão, que podem causar danos e reduzir a vida útil dos dispositivos. Além disso, ele melhora a eficiência energética, garantindo que os equipamentos operem dentro dos parâmetros ideais, o que pode levar a uma redução nos custos de energia e manutenção.<br>
<strong>Aplicações do Regulador de Tensão Trifásico</strong><br>
Os reguladores de tensão trifásicos são amplamente utilizados em indústrias, hospitais, centros de dados e grandes instalações comerciais. Eles são ideais para locais onde a estabilidade da tensão é crítica para o funcionamento seguro e eficiente de máquinas, sistemas de refrigeração, equipamentos médicos e outros dispositivos trifásicos.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>regulador de tensão trifásico</strong> envolve inspeções regulares para garantir que todos os componentes estejam funcionando corretamente. Isso inclui verificar conexões, limpar componentes internos e realizar testes de desempenho para identificar e corrigir qualquer anomalia. A manutenção preventiva é crucial para evitar falhas e garantir a longevidade do regulador.<br>
<strong>Dicas para Escolher um Regulador de Tensão Trifásico</strong><br>
Ao selecionar um regulador de tensão trifásico, considere a capacidade de carga, a precisão de regulação e as características de proteção, como proteção contra surtos e sobrecargas. Certifique-se de que o regulador seja adequado para o tipo de equipamentos que serão conectados e que ofereça uma operação confiável e estável.<br>
Invista em um regulador de tensão trifásico para garantir a estabilidade e proteção dos seus equipamentos industriais e comerciais, assegurando um fornecimento de energia eficiente e seguro.",

"<strong>Transformador de Corrente de Alta Tensão: Medição e Proteção Precisas</strong><br>
Um <strong>transformador de corrente de alta tensão</strong> é essencial para a medição precisa e a proteção dos sistemas elétricos em redes de alta tensão. Ele converte correntes elevadas em valores menores e mais gerenciáveis para serem usados em medidores, relés de proteção e outros equipamentos de monitoramento e controle.<br>
<strong>Vantagens do Transformador de Corrente de Alta Tensão</strong><br>
A principal vantagem de um transformador de corrente de alta tensão é a capacidade de fornecer medições precisas e seguras das correntes elétricas em sistemas de alta tensão. Ele isola os instrumentos de medição e proteção da alta tensão, aumentando a segurança operacional. Além disso, esses transformadores são projetados para suportar ambientes rigorosos e oferecer alta durabilidade.<br>
<strong>Aplicações do Transformador de Corrente de Alta Tensão</strong><br>
Os transformadores de corrente de alta tensão são utilizados em subestações, linhas de transmissão, redes de distribuição e em instalações industriais. Eles são essenciais para o monitoramento e controle de sistemas elétricos, permitindo a detecção de falhas, sobrecargas e outros problemas que podem afetar a confiabilidade e a segurança da rede elétrica.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>transformador de corrente de alta tensão</strong> envolve inspeções regulares para verificar o estado dos isoladores, conexões e a integridade estrutural. Testes de precisão e calibração também são necessários para garantir que o transformador continue a fornecer medições precisas. Limpezas periódicas e monitoramento de possíveis sinais de desgaste ou danos são cruciais para a operação contínua e segura.<br>
<strong>Dicas para Escolher um Transformador de Corrente de Alta Tensão</strong><br>
Ao selecionar um transformador de corrente de alta tensão, considere a precisão necessária, a capacidade de corrente, o nível de isolamento e as condições ambientais da instalação. Certifique-se de que o transformador atenda aos padrões e normas de segurança aplicáveis. Opte por fabricantes renomados que ofereçam suporte técnico e garantia de qualidade.<br>
Invista em um transformador de corrente de alta tensão para garantir a medição precisa e a proteção eficaz de seus sistemas elétricos, assegurando a segurança e a confiabilidade da rede.",

"<strong>Transformadores de Corrente e Potencial: Medição e Proteção em Sistemas Elétricos</strong><br>
Os <strong>transformadores de corrente e potencial</strong> são essenciais para a medição precisa e a proteção de sistemas elétricos em redes de alta e baixa tensão. Eles desempenham um papel crucial na conversão de valores elétricos elevados em níveis seguros e manejáveis, permitindo monitoramento e controle eficientes.<br>
<strong>Transformadores de Corrente</strong><br>
Os transformadores de corrente (TC) são usados para reduzir correntes altas a níveis mais baixos, adequados para instrumentos de medição e relés de proteção. Isso permite a medição segura e precisa das correntes em linhas de transmissão, subestações e instalações industriais. Além de proteger equipamentos de medição contra correntes excessivas, os TCs ajudam a detectar falhas e sobrecargas no sistema elétrico.<br>
<strong>Transformadores de Potencial</strong><br>
Os transformadores de potencial (TP) são projetados para reduzir a tensão de sistemas de alta tensão a valores menores, seguros para medição e proteção. Eles são fundamentais para a medição de tensão em redes elétricas, fornecendo dados precisos para a operação e controle do sistema. Os TPs são amplamente utilizados em subestações, permitindo a monitorização constante da tensão de linha.<br>
<strong>Vantagens e Aplicações</strong><br>
A principal vantagem desses transformadores é a segurança que oferecem ao isolar instrumentos de medição da alta tensão. Eles são essenciais em subestações, linhas de transmissão e distribuição, e em instalações industriais para garantir a precisão dos dados de medição e a proteção dos sistemas elétricos. Além disso, ajudam a manter a estabilidade e a eficiência das redes de energia.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção regular de <strong>transformadores de corrente e potencial</strong> inclui inspeções visuais, testes de precisão, limpeza e verificação das conexões elétricas. Testes de isolamento e calibração periódica são cruciais para garantir que os transformadores continuem a operar com precisão e segurança. A identificação precoce de desgastes ou falhas pode prevenir problemas maiores e prolongar a vida útil dos equipamentos.<br>
Invista em transformadores de corrente e potencial de alta qualidade para assegurar medições precisas e a proteção eficaz dos seus sistemas elétricos, garantindo a segurança e confiabilidade da sua rede.",

"<strong>Regulador Monofásico: Estabilidade e Proteção Elétrica</strong><br>
Um <strong>regulador monofásico</strong> é um dispositivo essencial para garantir a estabilidade da tensão elétrica em sistemas monofásicos, protegendo equipamentos contra variações e picos de energia. Ele é amplamente utilizado em residências, escritórios e pequenas indústrias para assegurar um fornecimento de energia constante e confiável.<br>
<strong>Vantagens do Regulador Monofásico</strong><br>
A principal vantagem de um regulador monofásico é a proteção que ele oferece aos equipamentos eletrônicos, prevenindo danos causados por sobretensões, subtensões e picos de energia. Além disso, ele melhora a eficiência energética, garantindo que os aparelhos funcionem dentro dos parâmetros ideais, o que pode prolongar sua vida útil e reduzir custos com manutenção.<br>
<strong>Aplicações do Regulador Monofásico</strong><br>
Os reguladores monofásicos são usados em uma variedade de aplicações, como em sistemas de iluminação, equipamentos de informática, aparelhos eletrodomésticos e sistemas de telecomunicações. Eles são ideais para locais onde a estabilidade da tensão é crítica para o desempenho e segurança dos dispositivos conectados.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>regulador monofásico</strong> é relativamente simples, envolvendo inspeções periódicas para verificar o estado dos componentes e assegurar que o dispositivo esteja operando corretamente. Limpezas regulares e testes de funcionamento ajudam a identificar problemas potenciais antes que eles afetem o desempenho do regulador.<br>
<strong>Dicas para Escolher um Regulador Monofásico</strong><br>
Ao escolher um regulador monofásico, considere a capacidade de carga, a precisão de regulação e a proteção contra surtos. Certifique-se de que o modelo escolhido seja adequado para o tipo de aparelhos que serão conectados e que ofereça características como proteção contra sobrecargas e curto-circuitos.<br>
Invista em um regulador monofásico para garantir a estabilidade e proteção de seus equipamentos elétricos, assegurando um fornecimento de energia confiável e seguro.",

"<strong>Regulador Trifásico: Estabilidade e Eficiência Energética</strong><br>
Um <strong>regulador trifásico</strong> é essencial para garantir a estabilidade da tensão elétrica em sistemas trifásicos, protegendo equipamentos industriais e comerciais contra variações e picos de energia. Ele é utilizado para assegurar um fornecimento de energia constante e eficiente, fundamental para o desempenho de máquinas e equipamentos de alta potência.<br>
<strong>Vantagens do Regulador Trifásico</strong><br>
A principal vantagem de um regulador trifásico é a proteção que oferece aos equipamentos contra flutuações de tensão, que podem causar danos e reduzir a vida útil dos dispositivos. Além disso, ele melhora a eficiência energética, garantindo que os equipamentos operem dentro dos parâmetros ideais, o que pode levar a uma redução nos custos de energia e manutenção.<br>
<strong>Aplicações do Regulador Trifásico</strong><br>
Os reguladores trifásicos são amplamente utilizados em indústrias, hospitais, centros de dados e grandes instalações comerciais. Eles são ideais para locais onde a estabilidade da tensão é crítica para o funcionamento seguro e eficiente de máquinas, sistemas de refrigeração, equipamentos médicos e outros dispositivos trifásicos.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>regulador trifásico</strong> envolve inspeções regulares para garantir que todos os componentes estejam funcionando corretamente. Isso inclui verificar conexões, limpar componentes internos e realizar testes de desempenho para identificar e corrigir qualquer anomalia. A manutenção preventiva é crucial para evitar falhas e garantir a longevidade do regulador.<br>
<strong>Dicas para Escolher um Regulador Trifásico</strong><br>
Ao selecionar um regulador trifásico, considere a capacidade de carga, a precisão de regulação e as características de proteção, como proteção contra surtos e sobrecargas. Certifique-se de que o regulador seja adequado para o tipo de equipamentos que serão conectados e que ofereça uma operação confiável e estável.<br>
Invista em um regulador trifásico para garantir a estabilidade e proteção dos seus equipamentos industriais e comerciais, assegurando um fornecimento de energia eficiente e seguro.",
];


// Criação de página
// Criação Mini site


// Criar página de produto mpi
$VetPalavrasProdutos = [
"capacitor-de-potencia",
"equipamentos-de-subestacao",
"regulador-de-tensao-monofasico",
"regulador-de-tensao-trifasico",
"transformador-de-corrente-alta-tensao",
"transformadores-de-corrente-e-potencial",
"regulador-monofasico",
"regulador-trifasico"
];

//Criar página de Serviço
$VetPalavrasInformacoes = [
"manutencao-de-transformadores-de-alta-tensao",
"reforma-de-transformadores"
];



// Numero do formulario de cotação
$formCotar = 170;



// Informações Geral.php

$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Bralux Transformadores';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'A melhor opção do seguimento';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'R. João Pinto, 1017';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Parque da Empresa';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'Mogi Mirim';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'SP';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : '13803-330';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-fixo.svg';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'comercial@braluxtransformadores.com';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : 'https://www.instagram.com/sanseitalhasoficial/';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';



?>


<!-- VARIAVEIS DE CORES -->

<style>
    :root {
        --azul-solucs: #2d2d2f;
        --second-color: #ffcc2a;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>