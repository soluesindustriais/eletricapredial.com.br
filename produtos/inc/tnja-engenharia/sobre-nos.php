<?
$h1         = 'Sobre nós';
$title      = 'Sobre nós';
$desc       = 'sobre nos';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Sobre nós';

include('inc/head.php');
?>

<style>
    .main-sobre-nos {
        justify-content: start;

        & h2 {
            font-size: 2.3rem;
        }

        & p {
            font-size: 1.2rem;
        }
    }

    <?
    include('css/header-script.css');
    include "$linkminisite" . "css/style.css";
    include "$linkminisite" . "css/mpi.css";
    include "$linkminisite" . "css/normalize.css";
    include "$linkminisite" . "css/aside.css";
    ?>
</style>
</head>

<body>

    <header id="nav-menu" aria-label="navigation bar">
        <? include "inc/header-dinamic.php" ?>
    </header>
    <?= $caminho ?>

    <main class="wrapper main-sobre-nos">
        <h2>Luci Comercio</h2>
        <p>Há anos atuando no mercado, a LUCI COMERCIO tem se destacado como uma empresa comprometida em oferecer produtos de alta qualidade e preços justos. Nosso compromisso vai além de simples transações comerciais; buscamos constantemente superar as expectativas de nossos clientes, garantindo não apenas produtos excelentes, mas também um atendimento excepcional.</p>
        <p>Nossa equipe dedicada trabalha incansavelmente para atender às necessidades específicas de cada cliente. Com um amplo portfólio de produtos e modelos, estamos preparados para oferecer soluções que se adequem perfeitamente aos seus requisitos. Desde materiais de construção até itens de decoração, temos tudo o que você precisa em um só lugar.</p>
        <p>Além da qualidade dos produtos, valorizamos a pontualidade. Cumprimos rigorosamente os prazos de distribuição acordados, garantindo que você receba seus pedidos no momento certo. Nosso compromisso com a excelência também se reflete em nosso atendimento ao cliente, sempre pronto para ajudar e oferecer as melhores soluções.</p>
        <p>Para tirar dúvidas, fazer sugestões ou realizar pedidos, entre em contato conosco pelo telefone 11 2093-8108 ou pelo WhatsApp 11 93364-7341. Estamos ansiosos para atendê-lo e proporcionar a melhor experiência de compra possível.</p>
        <p>Confie na LUCI COMERCIO para suprir suas necessidades com qualidade, compromisso e eficiência. Venha fazer parte da nossa história de sucesso!</p>
    </main>


    <? include('inc/footer.php'); ?>
</body>




</html>