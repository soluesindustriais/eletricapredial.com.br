
        <div class="grid">
            <div class="col-6">
                <div class="picture-legend picture-center">
                    <a href="<?= $url ?>imagens/produtos/produtos-01.webp" class="lightbox" title="<?= $h1 ?>" target="_blank">
                        <img class="lazyload" src="<?= $url ?>imagens/produtos/produtos-01.webp" alt="<?= $h1 ?>" title="<?= $h1 ?>" />
                    </a>
                    <strong>Imagem ilustrativa de <?= $h1 ?></strong>
                </div>
            </div>
            <div class="col-6">
                <div class="picture-legend picture-center">
                    <a href="<?= $url ?>imagens/produtos/produtos-02.webp" class="lightbox" title="<?= $h1 ?>" target="_blank">
                        <img class="lazyload" src="<?= $url ?>imagens/produtos/produtos-02.webp" alt="<?= $h1 ?>" title="<?= $h1 ?>" />
                    </a>
                    <strong>Imagem ilustrativa de <?= $h1 ?></strong>
                </div>
            </div>
        </div>