<section class="section">
    <div class="wrapper">
        <div class="partners-company">
            <h2>NOSSOS PRINCIPAIS CLIENTES</h2>
            <p>A CALD AÇO LTDA. conquistou uma sólida base de clientes devido à qualidade e ao profissionalismo dos nossos serviços. Confira alguns dos principais parceiros que confiam em nosso trabalho.</p>
            <a href="<?= $url ?>sobre-nos" title="Sobre nós">Saiba mais sobre nós</a>

        </div>
        <div style="display: grid; grid-template-columns: 1fr 1fr;">
            <div class="grid-item-mark"><img src="imagens/logos/logo-1.png" alt="img"></div>
            <div class="grid-item-mark"><img src="imagens/logos/logo-2.png" alt="img"></div>
            <div class="grid-item-mark"><img src="imagens/logos/logo-3.png" alt="img"></div>
            <div class="grid-item-mark"><img src="imagens/logos/logo-4.png" alt="img"></div>
            <!-- <div class="grid-item-mark"><img src="imagens/logos/logo-5.png" alt="img"></div>
            <div class="grid-item-mark"><img src="imagens/logos/logo-6.png" alt="img"></div>
            <div class="grid-item-mark"><img src="imagens/logos/logo-7.png" alt="img"></div>
            <div class="grid-item-mark"><img src="imagens/logos/logo-8.webp" alt="img"></div>
            <div class="grid-item-mark"><img src="imagens/logos/logo-9.webp" alt="img"></div> -->
        </div>
    </div>
</section>