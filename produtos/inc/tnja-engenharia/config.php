<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'TNJA Montagens';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Trabalhamos com a elaboração de projetos elétricos';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '-', $tituloCliente);

//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";


include("$linkminisite" . "conteudos-minisite.php");


// Criar página de produto mpi
$VetPalavrasProdutos = [
    "cabine-primaria",
    "construcao-de-rede-eletrica",
    "instalacao-de-aterramento",
    "instalacao-industrial-eletrica",
    "instalacao-spda",
    "manutencao-de-cabine-primaria",
    "projeto-instalacao-eletrica",
    "projetos-de-instalacao-eletrica",
    "protecao-contra-descargas-atmosfericas",
    "servicos-de-aterramento",
];

$VetPalavrasOriginais = [
    "cabine-primária",
    "construção-de-rede-elétrica",
    "instalação-de-aterramento",
    "instalação-industrial-elétrica",
    "instalação-spda",
    "manutenção-de-cabine-primária",
    "projeto-instalação-elétrica",
    "projetos-de-instalação-elétrica",
    "proteção-contra-descargas-atmosféricas",
    "serviços-de-aterramento",
];

//Criar página de Serviço
/* $VetPalavrasInformacoes = [

]; */


// Numero do formulario de cotação
$formCotar = 302; // Colocar aqui;

// -> Feito no config

// Informações Geral.php
$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'TNJA Montagens Elétricas';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'Trabalhamos com a elaboração de projetos elétricos';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'R. Cananéia, 73';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Vila Della Piazza';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'Jundiaí';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'SP';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : 'CEP: 13207-604';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-2.webp';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'tnja@grupoengemac.com';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : '';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';
$telefoneCliente = ($clienteAtivo == 'inativo') ? '' : '(11) 4582-1675';

?>

<!-- VARIAVEIS DE CORES -->

<style>
    :root {
        --azul-solucs: rgba(27, 39, 50, 1);
        --second-color: #005825;
        --cor-dourada: #2d2d2f;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>