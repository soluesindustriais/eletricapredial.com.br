<section class="product-image-bottom">
    <?php
    foreach ($menuItems as $value_products => $key_products) {
        if (isset($key_products["submenu"])) {
            $pathImgProduct = $key_products["submenu"][$h1]["url"];
            for ($d = 1; $d < 3; $d++) {
                if (file_exists($prefix_includes . "imagens/informacoes/" . $pathImgProduct . "-$d.webp")) {
                    $pathImgProductInsert = "$pathImgProduct-$d.webp";
                    echo '
                    <div class="product-image-bottom-item">
                        <a href="' . $prefix_includes . "imagens/informacoes/". $pathImgProductInsert . '" class="lightbox" title="'.$h1.'">
                            <img class="product-image-aside-item-img" src="' . $prefix_includes . 'imagens/informacoes/'.$pathImgProductInsert.'" alt="Imagem de '.$h1.'">
                        </a>
                    </div>
                    ';
                }
            }
        }
    }
    ?>
</section>