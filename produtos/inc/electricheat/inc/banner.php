<!-- Aqui você deve alterar os títulos e mensagens do banner e a imagem do background, e o link dos cliques -->
	<?php if (!$isMobile) : ?>
		<div class="slick-banner">
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.7) 50%, rgba(36, 36, 36, 0.7) 50%), url('<?= $url ?>imagens/banner/banner-1.webp')">
				<div>
					<div class="content-banner justify-content-evenly row">
						<div>
							<h1>Soluções Inovadoras em Aquecimento Elétrico Industrial</h1>
							<p>Excelência em Produtos e Serviços para Ambientes Industriais e Atmosferas Explosivas</p>
							<a class="btn" href="<?= $url ?>catalogo" title="Página de produtos">Clique</a>
						</div>
						<div>
							<img src="<?= $url ?>imagens/clientes/logo-categoria.png" alt="" title="" class="slick-thumb">
						</div>
					</div>
				</div>
			</div>
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.5) 50%, rgba(36, 36, 36, 0.5) 50%), url('<?= $url ?>imagens/banner/banner-2.webp')">
				<div class="content-banner">
					<h2>Especialistas em Aquecimento Elétrico para Indústria</h2>
					<p>Segurança, Qualidade e Inovação em Soluções para Atmosferas Explosivas</p>
					<a class="btn" href="<?= $url ?>sobre-nos" title="Página sobre nós">Clique</a>
				</div>
			</div>
	
		</div>
	<?php endif; ?>