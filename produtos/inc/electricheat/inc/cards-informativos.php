

<section class="wrapper card-informativo">

    <h2>Nossos principais focos</h2>
        <div class="cards-informativos">
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-map"></i>
            <h3>Missão</h3>
            <p>Fornecer soluções inovadoras e seguras em aquecimento elétrico industrial, atendendo às necessidades de nossos clientes em ambientes comuns e atmosferas explosivas.</p>
        </div>
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-eye"></i>
            <h3>Visão</h3>
        <p>Ser a principal referência no Brasil em aquecimento elétrico industrial, destacando-se pela qualidade de nossos produtos e serviços, pela inovação em soluções para atmosferas explosivas e pelo compromisso com a satisfação de nossos clientes.</p>
    </div>
    <div class="cards-informativos-infos">
        <i class="fa-solid fa-handshake"></i>
        <h3>Valores</h3>
        <p>Segurança, priorizando a integridade das pessoas, equipamentos e instalações em todos os processos. Qualidade, garantindo produtos e serviços que atendam aos mais altos padrões do mercado.</p>
    </div>
</div>
</section>
