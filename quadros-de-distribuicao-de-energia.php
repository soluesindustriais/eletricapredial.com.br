<? $h1 = "Quadros de distribuição de energia"; $title  = "Quadros de distribuição de energia"; $desc = "Os $h1 são equipamentos utilizados no dia a dia da indústria, na Elétrica Predial você realiza cotações com as melhores marcas do mercado. Acesse!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>

<body>
    <? include('inc/topo.php');?>
    <div class="wrapper">
        <main>
            <div class="content">
                <section>
                    <br class="clear">
                    <?=$caminhopaineis?>
                    <br class="clear">
                    <br class="clear" />
                    <div>
                        .
                    </div>
                    <div class="mpi-produtos">
                        <div class="col-1"><br></div>
                        <div class="col-5">
                            <img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg"
                                alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos"
                                data-anime="in">
                        </div>
                        <div class="col-6 content-mobile">
                            <h1 data-anime="right">
                                <?=$h1?>
                            </h1>
                            <p data-anime="in">
                                Um quadro de distribuição de energia é um equipamento elétrico que possui a missão de
                                receber energia elétrica de variadas fontes de alimentação e compartilhá-las a um ou
                                mais circuitos. O <strong>quadro de energia</strong> abrigar a conexão de
                                <strong>condutores elétricos</strong> interligados a eles, com a função de distribuir a
                                eletricidade aos diversos circuitos.
                                Toda a energia que passa pela sua casa, por via de interruptores e tomadas, em algum
                                momento esteve em contato com o <strong>quadro de medição</strong>. Esse aparelho está
                                ligado a equipamento o qual mede o consumo mensal de energia, a partir desse contato a
                                energia entra em sua residência através de um ramal até o quadro de distribuição de onde
                                partirão os circuitos que irão abastecer os pontos de luz espalhados pelo local.
                                Qualquer instalação elétrica, deve ser feita por profissionais experientes e
                                qualificados, que atendam as especificações previstas na NBR 5410 (norma que regulamenta
                                as instalações elétricas em baixa tensão) e NR-10 (segurança em instalações e serviços
                                em eletricidade).
                            </p>
                            <p>Você pode se interessar também por <a target='_blank'
                                    title='Painel eletrônico cronometro'
                                    href="https://www.eletricapredial.com.br/painel-eletronico-cronometro">
                                    Painel eletrônico cronometro</a>. Veja mais detalhes ou solicite um <b>orçamento
                                    gratuito</b> com
                                um dos fornecedores disponíveis!</p>
                            <button class="botao-cotar btn-produto" title="quadros de distribuicao de energia">Orçamento Grátis</button>
                            <br class="clear">
                        </div>
                    </div>
                    <br class="clear">
                    <br class="clear">
                    <div class="mpi-produtos-2">
                        <br class="clear">
                        <div class="col-1"><br></div>
                        <div class="col-6 content-mobile">
                            <h2>Quadro elétrico</h2>
                            <p>O mercado apresenta diversos tipos de quadros elétricos, que são conhecidos como centros
                                de distribuição de energia. Cada tipo de quadro atende uma função específica, porém os
                                que são mais utilizados são os centrinhos.
                                Os centrinhos de distribuição são chamados de <strong>quadros de força</strong>,
                                fabricados em material termoplástico, com enorme aplicação em obras de condomínios e
                                hotéis que utilizam quadros típicos para distribuição de cargas em apartamentos e
                                suítes. Possuem o preço bastante reduzido devido ao usual volume de fornecimento.</p>
                            <h2>Disjuntores</h2>
                            <p>Os disjuntores também são conhecidos como chaves, e estão presentes em todos os quadros
                                de energia, servem ligar ou desligar o padrão de energia, ou mesmo as chaves de
                                segurança dentro dos painéis e quadros de distribuição. A principal missão do disjuntor
                                é ser um componente para proteção e segurança, mas devida sua composição mecânica
                                possibilita o seccionamento de circuitos ele também é utilizado como elementos para se
                                ligar e desligar circuitos e cargas.
                                O disjuntor é elaborado com a intenção de suportar uma determinada corrente elétrica,
                                caso aconteça um pico de corrente ou mesmo um curto circuito que eleve consideravelmente
                                a corrente acima do limite suportado pelo equipamento, o mesmo interrompe o circuito,
                                assegurando a proteção de todos os elementos que compunham esse circuito, após sanado
                                esse sinistro o disjuntor pode ser rearmado para a continuidade do funcionamento deste
                                circuito.</p>
                        </div>
                        <div class="col-4">
                            <img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg"
                                alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico">
                        </div>
                        <div class="col-1"><br></div>
                    </div>
                    <hr>
                    <div data-anime="in">
                        <?include('inc/tabela.php');?>
                    </div>
                    <!-- 				<span class=" btn-produto" >PDF </span> -->

                    <br class="clear">
                    <hr>
                    <div class="wrapper-fixa">
                        <p class="txtcenter">
                            <br class="clear">
                            <?=$desc?>
                        </p>

                        <? include('inc/galeria-fixa-mpi.php');?>

                        <button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar" title="quadros de distribuicao de energia">Orçamento
                            Grátis</button>
                    </div>

                    <? include('inc/form-mpi.php');?>

                </section>
            </div>
        </main>
    </div>
    <!-- .wrapper -->
    <? include('inc/footer.php');?>
</body>

</html>