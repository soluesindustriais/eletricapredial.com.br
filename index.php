<?
$h1         = 'Eletrica Predial';
$title      = 'Início';
$desc       = 'Quadro de Distribuição Elétrica - Encontre diversos fabricantes de Pisos e faça uma cotação agora mesmo. É gratis!';
// $key        = 'pisos de epoxi, piso tatil, pintura de piso, pisos impermeabilizantes';
$var        = 'Início';
include('inc/head.php');
?>
<style>
<?php include "css/index.css"?>
</style>
</head>

   <? include('inc/topo.php'); ?>
   
<body>
   <div class="theme-default">
      <div class="wrapper-intro">
         <? include('inc/topo.php'); ?>
      </div>
      <?php include "inc/banner-index.php"?>
   </div>
   <main>
      <section>


         <section class="wrapper-main">
            <div class="main-center">
               <div class=" quadro-2 ">
                  <h2>Equipamentos Elétricos</h2>
                  <div class="div-img">
                     <img src="<?= $url ?>imagens/quadro-de-distribuicao-qta.jpg" alt="Quadro de Distribuição de QTA" title="Quadro De Distribuição de QTA" class="imagen-principal">
                     <p> A energia elétrica é a força que move o mundo nos dias atuais, é a partir dela que empresas funcionam. Porém, para que toda a operação seja realizada de forma eficiente, é preciso que todos os componentes elétricos estejam trabalhando da melhor forma possível.
                        Na maioria das empresas, existem uma série de dispositivos executando atividades diariamente, para que eles tenham o melhor funcionamento, adquira os itens para compor seu projeto elétrico em uma ótima loja de material elétrico, e garanta mais segurança a sua empresa.</p>
                  </div>
                  <p class="p-tiny">QTA Predial</p>
               </div>
               <div class=" incomplete-box">
                  <ul>
                     <li>
                        <p>Hoje em dia as pessoas reconhecem como as instalações elétricas são partes essenciais dentro do projeto de uma empresa, mas para que tudo esteja funcionando conforme o planejado, é sempre importante adquirir seus dispositivos em uma loja de elétrica de confiança.
                           Como muitas pessoas não possuem disponibilidade de tempo para pesquisar os produtos em vários fornecedores diferentes, atualmente já existem as lojas de material elétrico online, elas agilizam o processo de cotação e garantem o melhor preço sempre!</p>
                        <p>Entre os diferentes tipos de dispositivos elétricos, destacam-se:</p>
                     <li><i class="fas fa-bolt"></i> Painel Elétrico</li>
                     <li><i class="fas fa-bolt"></i> Painel Eletrônico</li>
                     <li><i class="fas fa-bolt"></i>Manutenção</li>
                     <li><i class="fas fa-bolt"></i> Quadro Elétrico</li>
                  </ul>
                  <a class="btn-4" href="<?= $url ?>produtos-categoria" title="Mais Informações">Nossos produtos</a>
               </div>
            </div>
            <section class="faixa-de-icone">
               <div class="wrapper-index">
                  <div class="index-icon-card">
                     <span></span>
                     <i class="fas fa-bolt"></i>
                     <p>Eficiência Energética</p>
                  </div>
                  <div class="index-icon-card">
                     <span></span>
                     <i class="fas fa-dollar-sign"></i>
                     <p>Compare preços</p>
                  </div>
                  <div class="index-icon-card">
                     <span></span>
                     <i class="fas fa-handshake"></i>
                     <p>Manutenção</p>
                  </div>
                  <div class="index-icon-card">
                     <span></span>
                     <i class="fas fa-clock"></i>
                     <p>Economize tempo</p>
                  </div>
               </div>
            </section>
         </section>
         <section class="wrapper-img">
            <div class="txtcenter">
               <h2>Produtos <b>Relacionados</b></h2>
            </div>
            <div class="contentIcons">
               <figure>
                  <a href="<?= $url ?>painel-eletrico-categoria">
                     <img src="<?= $url ?>imagens/comprar-quadro-eletrico.jpg" alt="Comprar quadro Elétrico" title="Comprar quadro Elétrico" class="image">
                     <div class="fig-img">
                        <div>Painel Elétrico</div>
                     </div>
                  </a>
               </figure>
               <figure>
                  <a href="<?= $url ?>painel-eletronico-categoria">
                     <img src="<?= $url ?>imagens/comprar-painel-eletronico.jpg" alt="Comprar Painel Eletrônico" title="Comprar Painel Eletrônico" class="image">
                     <div class="fig-img">
                        <div>Painel Eletrônico</div>
                     </div>
                  </a>
               </figure>
               <figure>
                  <a href="<?= $url ?>quadro-de-transferencia-categoria">
                     <img src="<?= $url ?>imagens/comprar-quadro-de-distribuicao.jpg" alt="Comprar Quadro de Distribuição" title="Comprar Quadro de Distribuição" class="image">
                     <div class="fig-img">
                        <div>Quadro de Transferência</div>
                     </div>
                  </a>
               </figure>
               <figure>
                  <a href="<?= $url ?>quadro-eletrico-categoria">
                     <img src="<?= $url ?>imagens/comprar-quadro-eletronico.jpg" alt="Comprar Quadro Eletrônico" title="Comprar Quadro Eletrônico" class="image">
                     <div class="fig-img">
                        <div>Quadro Elétrico</div>
                     </div>
                  </a>
               </figure>

            </div>
         </section>
         <section class="wrapper-destaque">
            <div class="wrapper">
               <div class="destaque txtcenter">
                  <h2>Galeria de <b> Produtos</b></h2>
                  <div class="center-block txtcenter">
                     <ul class="gallery">
                        <li><a href="<?= $url ?>imagens/comprar-painel-eletrico.jpg" class="lightbox" title="Comprar Painel Eletrico">
                              <img src="<?= $url ?>imagens/thumbs/comprar-painel-eletrico.jpg" alt="Comprar Painel Elétrico" title="Comprar Painel Elétrico">
                           </a>
                        </li>
                        <li><a href="<?= $url ?>imagens/quadro-de-distribuicao-eletrica.jpg" class="lightbox" title="Quadro de Distribuição">
                              <img src="<?= $url ?>imagens/thumbs/quadro-de-distribuicao-eletrica.jpg" alt="Quadro de Distribuição Elétrica" title="Quadro de Distribuição Elétrica">
                           </a>
                        </li>
                        <li><a href="<?= $url ?>imagens/montador-de-quadro-eletronico.jpg" class="lightbox" title="Montador de Quadro Eletrônico">
                              <img src="<?= $url ?>imagens/thumbs/montador-de-quadro-eletronico.jpg" alt="Montador de Quadro Eletrônico" title="Montador de Quadro Eletrônico">
                           </a>
                        </li>
                        <li><a href="<?= $url ?>imagens/painel-eletrico-residencial.jpg" class="lightbox" title="Painel Elétrico">
                              <img src="<?= $url ?>imagens/thumbs/painel-eletrico-residencial.jpg" alt="Painel Elétrico Residencial" title="Painel Eletrônico">
                           </a>
                        </li>
                        <li><a href="<?= $url ?>imagens/quadro-eletrico-preco.jpg" class="lightbox" title="Quadro Elétrico">
                              <img src="<?= $url ?>imagens/thumbs/quadro-eletrico-preco.jpg" alt="Quadro Elétrico" title="Quadro Elétrico">
                           </a>
                        </li>
                        <li><a href="<?= $url ?>imagens/comprar-painel-de-senha.jpg" class="lightbox" title="Comprar Painel de Senha">
                              <img src="<?= $url ?>imagens/thumbs/comprar-painel-de-senha.jpg" alt="Comprar Painel de Senha" title="Comprar Painel de Senha">
                           </a>
                        </li>
                        <li><a href="<?= $url ?>imagens/quadro-eletrico-industrial.jpg" class="lightbox" title="Quadro elétrico industrial">
                              <img src="<?= $url ?>imagens/thumbs/quadro-eletrico-industrial.jpg" alt="Quadro elétrico industrial" title="Quadro elétrico industrial">
                           </a>
                        </li>
                        <li><a href="<?= $url ?>imagens/quadro-eletrico-de-distribuicao.jpg" class="lightbox" title="Quadro Elétrico de Distribuição">
                              <img src="<?= $url ?>imagens/thumbs/quadro-eletrico-de-distribuicao.jpg" alt="Quadro Elétrico de Distribuição" title="Quadro Elétrico de Distribuição">
                           </a>
                        </li>
                        <li><a href="<?= $url ?>imagens/comprar-quadros-eletronicos.jpg" class="lightbox" title="Comprar Quadros Eletônicos">
                              <img src="<?= $url ?>imagens/thumbs/comprar-quadros-eletronicos.jpg" alt="Comprar Quadros Eletônicos" title="Comprar Quadros Eletônicos">
                           </a>
                        </li>
                        <li><a href="<?= $url ?>imagens/quadro-eletronico-preco.jpg" class="lightbox" title="Quadro Eletrônico Preço">
                              <img src="<?= $url ?>imagens/thumbs/quadro-eletronico-preco.jpg" alt="Quadro Eletrônico Preço" title="Quadro Eletrônico Preço">
                           </a>
                        </li>
                     </ul>
                     <br class="clear">
                  </div>
               </div>

               <h3 class="txtcenter">Produtos mais vendidos</h3>
               <ul class="thumbnails-mod1">
                  <li>
                     <a href="<?= $url; ?>painel-de-led" title="Painel de led"><img src="<?= $url; ?>imagens/painel/thumbs/painel-01.jpg" alt="Painel de led" title="Painel de led" /></a>
                     <h2><a href="<?= $url; ?>painel-de-led" title="Painel de led">Painel de led</a></h2>
                  </li>
                  <li>
                     <a href="<?= $url; ?>quadro-de-distribuicao" title="Quadro de distribuição"><img src="<?= $url; ?>imagens/quadro-de-transferencia/thumbs/quadro-de-transferencia-2.jpg" alt="Quadro de distribuição" title="Quadro de distribuição" /></a>
                     <h2><a href="<?= $url; ?>quadro-de-distribuicao" title="Quadro de distribuição">Quadro de distribuição</a></h2>
                  </li>
                  <li>
                     <a href="<?= $url; ?>painel-de-controle" title="Painel de controle"><img src="<?= $url; ?>imagens/painel/painel-11.jpg" alt="Painel de controle" title="Painel de controle" /></a>
                     <h2><a href="<?= $url; ?>painel-de-controle" title="Painel de controle">Painel de controle</a></h2>
                  </li>
                  <li>
                     <a href="<?= $url; ?>quadro-de-energia" title="Quadro de energia"><img src="<?= $url; ?>imagens/quadro-eletrico/thumbs/quadro-eletrico-01.jpg" alt="Quadro de energia" title="Quadro de energia" /></a>
                     <h2><a href="<?= $url; ?>quadro-de-energia" title="Quadro de energia">Quadro de energia</a></h2>
                  </li>
               </ul>


            </div>
            <br class="clear">
         </section>
      </section>
   </main>
   <? include('inc/footer.php'); ?>
</body>

</html>