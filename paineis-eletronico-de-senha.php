<? $h1 = "Painel Eletronico de Senha"; $title  = "Painel de Senha | Elétrica Predial"; $desc = "Muito utilizado em recepções, os painéis eletrônico de senha são as melhores opções para você. Não perca essa oportunidade de adquirir agora mesmo o seu, acesse!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/painel-eletronico-1.jpg" alt="Painel Eletrônico" title="Painel Eletrônico" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Painel eletrônico é um termo muito abrangente, mas que na grande maioria dos casos está fazendo referência ao dispositivo que exibe informações através de LEDs. Muito popular no Brasil, o <strong>painel de chamada</strong> está presente no dia a dia da maioria das pessoas, seja em salas de recepção, consultórios médicos, cartórios, restaurantes e diversos outros estabelecimentos.
						É através da <strong>máquina de senha eletrônica</strong> que a comunicação em ambientes grandes consegue ser feita de maneira mais organizada e sem causar transtorno para nenhuma das partes. Atualmente, existem diversos modelos de painel eletrônico, cada um deles foi adaptado de modo a atender uma determinada demanda, os principais tipos são:</p>
						<ul>
							<li>Painel eletrônico de 1 linha;</li>
							<li>Painel eletrônico de 2 linhas;</li>
							<li>Painel eletrônico multilinhas.</li>
						</ul>
						<p>
							
							Sendo o primeiro deles mais recomendado para quem deseja expor frases pequenas e diretas, o segundo para pessoas que desejam personalizar de alguma forma o texto a ser exibido, e por fim, temos o modelo mais utilizado, que permite uma personalização do tamanho e das informações transmitidas.
							
						</p>
					</p>
					<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
					<br class="clear">
				</div>
			</div>
			<br class="clear">
			<br class="clear">
			<div  class="mpi-produtos-2">
				<br class="clear">
				<div class="col-1"><br></div>
				<div class="col-6 content-mobile">
					<h2>Painel de senha</h2>
					<p>É importante que antes de adquirir algum modelo de <strong>painel de senha digital</strong>, o consumidor saiba o local onde o mesmo ficará exposto, isso pode evitar grandes problemas futuros com manutenção e consertos. Por exemplo, em casos onde o equipamento ficará exposto a ações do tempo, como chuva e sol intenso, busque modelos que já possuam uma certa resistência a esse tipo de adversidade.
					Os <strong>painéis eletrônicos</strong> são equipamentos com alto poder de impacto visual, baixo consumo de energia e com uma grande variedade de recursos gráficos e efeitos visuais, trazendo benefícios para os mais diversos segmentos.</p>
					<h2>Placa de LED</h2>
					<p>Além de todas as funcionalidades e finalidades que as placas de LED oferecem, elas ainda são utilizadas, porém com uma escala menor, em outros ambientes. Um bom exemplo a ser citado, é o do <strong>placar eletrônico</strong>, um equipamento tradicionalmente utilizado nos mais diversos tipos de esportes e que possui a mesma configuração de painel tradicional.
					Agora é fácil entender como esse dispositivo se tornou tão fundamental em empresas de todos os segmentos, ele pode proporcionar uma melhor organização de ambientes grandes ou com fluxo intenso de pessoas e quando necessário ajudar na publicidade e divulgação de um determinado estabelecimento.</p>
				</div>
				<div class="col-4">
					<img src="<?=$url?>imagens/img-produtos/painel-eletronico-2.jpg" alt="Painel Eletrônico" title="Painel Eletrônico" >
				</div>
				<div class="col-1"><br></div>
			</div>
			<hr>
			<div  data-anime="in">
				<?include('inc/tabela.php');?>
			</div>
			<!-- 				<span class=" btn-produto" >PDF </span> -->
			
			<br class="clear">
			<hr>
			<div class="wrapper-fixa">
				<p class="txtcenter">
					<br class="clear">
					<?=$desc?>
				</p>
				
				<? include('inc/galeria-fixa-eletronico.php');?>
			</div>
			
			<? include('inc/form-mpi.php');?>
			
		</section>
	</div>
</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>