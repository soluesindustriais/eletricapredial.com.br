<? $h1 = "Quadros para transferências automática"; $title  = "Chave de Transferência Automática para Gerador | Elétrica Predial"; $desc = "Os quadros de transferência automática, ou QTA, são equipamentos de backup importantes no ramo industrial. Acesse o site e orce agora mesmo o seu, aproveite!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-1.jpg" alt="Empresas de Painéis Elétricos" title="Empresas de Painéis Elétricos" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							Muitas empresas, sejam elas do ramo industrial ou não, já enfrentaram problemas por conta da falta de energia. O colaborador está realizando uma atividade importante quando de repente acaba a luz e tudo que foi desenvolvido até o momento acaba se perdendo.
							Para solucionar esse problema é que a instalação de geradores se tornaram tão corriqueiras, porém transferir um equipamento de uma fonte de energia para outra automaticamente não é uma tarefa simples, isso exige a instalação de um dispositivo especial chamado <strong>painel qta</strong>.
							Além de fazer a transferência de uma fonte para a outra, ele possui mais uma função que auxilia a empresa, quando a luz retorna o painel continuará funcionando por um tempo pré estabelecido, desta maneira a energia no local se mantém estável sem causar grandes problemas ou impactos, e o cliente não sofrerá com idas e vindas de luz.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						
						<h2>Chave de Transferência Automática</h2>
						<p>O <strong>QTA gerador</strong> é um equipamento é ideal para quem trabalha com pressões negativas, irrigação de solo, aviários e ordenhas, mas também para o fornecimento de energia elétrica tanto para as indústrias pesadas quanto para empresas de qualquer porte, residências, eventos e o comércio em geral.
						Quando o QTA está parametrizado no modo automático e inadvertidamente acontece a interrupção da energia elétrica, um relé auxiliar é acionado ativando a partida do grupo gerador. Desta forma, todo o circuito elétrico é abastecido automaticamente. Entretanto, se alguma falha for identificada após sucessivas tentativas, o bloqueio automático é ligado, fazendo com que o conjunto gerador seja preservado. Esta quantidade de religamentos podem ser parametrizados de acordo com aplicação e respeitando as normas de segurança.</p>
						<h2>QTA para Gerador</h2>
						<p>O quadro de transferência automática deve ser implementado em sua empresa por um especialista no assunto e que acima de tudo, entenda o funcionamento do equipamento e conheça as instalações do cliente, só dessa forma será possível instalar um dispositivo 100% personalizado.
						Não perca tempo entrando em vários sites para conseguir realizar uma cotação, confie em um dos maiores portais voltados para a indústria e encontre os melhores <strong>gerador com QTA</strong> com um clique.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/empresas-de-paineis-eletricos-2.jpg" alt="Empresas de Painéis Elétrico" title="Empresas de Painéis Elétrico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-mpi.php');?>
<button id="btnOrcamento" class="btn-cotar-fixed meta-orc btn-produto botao-cotar">Orçamento Grátis</button>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>