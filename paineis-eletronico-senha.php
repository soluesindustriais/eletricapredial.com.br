<? $h1 = "Painéis eletrônicos senha"; $title  = "Painel de Senha Digital | Elétrica Predial"; $desc = "Melhore o atendimento em seu comércio com o painel eletrônico de senha. Acesse o site da Elétrica Predial e confira todas as vantagens deste produto!"; $key  = "Venda de quadros e painéis elétricos, Quadro de energia"; include('inc/quadro-eletrico/quadro-eletrico-linkagem-interna.php'); include('inc/head.php');  ?>
<!-- Tabs Regiões -->
<script defer src="
<?=$url?>js/organictabs.jquery.js">
</script>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main >
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".nova-api", "titulo": "h1", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>		
<div class="content">
			<section>
				<br class="clear">
				<?=$caminhopaineis?>
				<br class="clear">
				<br class="clear">
				<br class="clear" />
				<div class="mpi-produtos">
					<div class="col-1"><br></div>
					<div class="col-5">
						<img src="<?=$url?>imagens/img-produtos/painel-eletronico-1.jpg" alt="painel-eletronico-comprar" data-anime="in">
					</div>
					<div class="col-6 content-mobile">
						<h1 data-anime="right">
						<?=$h1?>
						</h1>
						<p data-anime="in">
							O painel eletrônico de senha é um equipamento muito importante, pois auxilia na organização e gerenciamento das filas em ambientes que atendem grande quantidade de pessoas, assegurando o conforto e segurança aos clientes que aguardam atendimento, possibilitando que eles não necessitem ficar o tempo todo ao redor do balcão esperando e nem que os funcionários tenham que gritar ou ir atrás do cliente.
							Assim que o cliente chega ao estabelecimento em que deseja ser atendido, ele deve retirar uma senha, normalmente composta de letras e números. A partir dessa senha o cliente será chamado ao balcão para ser atendido e solucionar suas dúvidas.
							O produto apresenta vantagens significativas para o local onde está aplicado, além de que, o seu processo de instalação é muito simples, basta colocar o fio condutor de energia do equipamento a uma tomada no local, após esse procedimento, basta posicionar o painel em uma parede, de preferência de concreto, e parafusa-lo para que não caia com o decorrer do tempo. É importante sempre colocar o painel no ângulo correto para que todas as pessoas do ambiente consigam localizá-lo com facilidade.
						</p>
						<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
						<br class="clear">
					</div>
				</div>
				<br class="clear">
				<br class="clear">
				<div  class="mpi-produtos-2">
					<br class="clear">
					<div class="col-1"><br></div>
					<div class="col-6 content-mobile">
						<h2>Painel de Senha</h2>
						<p>O painel de senha é o mais comum entre os <strong>painéis eletrônicos</strong>. Por meio de 3 dígitos, ele permite chamar as pessoas por ordem de chegada, de maneira sequencial. É o sistema indicado para locais que tenham um balcão de atendimento, como açougues, farmácias, padarias e comércio em geral. Nesse sistema tudo o que o atendente necessita fazer é apertar o botão do controle de chamada de senhas, assim o painel emite um sinal sonoro e mostra o número da próxima senha. </p>
						<h2>Painel eletrônico preço</h2>
						<p>Se o cliente deseja realizar a compra do painel eletrônico preço justo, ele deve fazer uma pesquisa de mercado e encontrar a marca que o melhor atenda. É imprescindível que o fabricante do painel ofereça o total apoio para o cliente solucionar todas as suas dúvidas, referente a modelos de painéis que melhor se adequem a suas necessidades, como o cliente pode configurar o mesmo, e se caso ocorra algum defeito onde ele possa realizar a manutenção de painéis eletrônicos.</p>
					</div>
					<div class="col-4">
						<img src="<?=$url?>imagens/img-produtos/painel-eletronico-2.jpg" alt="onde-comprar-painel-eletronico" >
					</div>
					<div class="col-1"><br></div>
				</div>
				<hr>
				<div  data-anime="in">
					<?include('inc/tabela.php');?>
				</div>
				<!-- 				<span class=" btn-produto" >PDF </span> -->
				
				<br class="clear">
				<hr>
				<div class="wrapper-fixa">
					<p class="txtcenter">
						<br class="clear">
						<?=$desc?>
					</p>
					
					<? include('inc/galeria-fixa-eletronico.php');?>
<span class="botao-cotar btn-produto" >Orçamento Grátis </span>
				</div>
				
				<? include('inc/form-mpi.php');?>
				
			</section>
		</div>
	</main>
</div>
<!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>